# ----------------------------------------------------------------------
# Copyright (C) 2019  opendta@gmx.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

"""Setup internationalization support."""
import gettext
import os
from . import pkg_info

# domain name
domain = str(pkg_info['name'])  # mypy wants have an explicit type conversion!?
# where to search for translations
localedir = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'locale')
# create translation object
translate = gettext.translation(domain, localedir=localedir, fallback=True)

# shortcut to mark strings
_ = translate.gettext

# redefine gettext.gettext for argparse
gettext.gettext = translate.gettext

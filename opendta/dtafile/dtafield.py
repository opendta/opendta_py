# ----------------------------------------------------------------------
# Copyright (C) 2020  opendta@gmx.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

from dataclasses import dataclass, field
from typing import List, Any, Optional

from .binarydata import BinaryData


@dataclass()
class DtaRGB(object):
    """Class to hold color values."""
    r: int = 0
    g: int = 0
    b: int = 0

    def __str__(self) -> str:
        return '#{:02X}{:02X}{:02X}'.format(
            self.r, self.g, self.b
        )


@dataclass()
class DtaField(object):
    """Define base class for fields in DTA file."""
    category: str = ''
    name: str = ''

    def __len__(self) -> int:
        return 0  # pragma: no cover

    def __str__(self) -> str:
        return 'category="{}" name="{}"'.format(self.category, self.name)

    def read(self, data: BinaryData) -> List[Any]:
        return []  # pragma: no cover

    def get_names(self) -> List[str]:
        return [self.name]


@dataclass()
class DtaFieldProp(DtaField):
    """Extend DtaField with additional properties."""
    visible: bool = True
    color: Optional[DtaRGB] = None
    factory_only: bool = False

    def __str__(self) -> str:
        return '{} color={} visible={} factory_only={}'.format(
            super().__str__(),
            str(self.color),
            '1' if self.visible else '0',
            '1' if self.factory_only else '0'
        )


@dataclass()
class DtaFieldTimestamp(DtaField):
    """Define class for DTA timestamp."""

    def __len__(self) -> int:
        return 1

    def __str__(self) -> str:
        return 'timestamp: {}'.format(super().__str__())

    def read(self, data: BinaryData) -> List[int]:
        return [data.get_uint32()]


@dataclass()
class DtaFieldUnknown(DtaField):
    length: int = 0

    def __len__(self):
        return 0

    def __str__(self) -> str:
        return 'unused_data: length={}'.format(self.length)

    def read(self, data: BinaryData) -> List[Any]:
        data.unused_data(self.length)
        return []


@dataclass()
class DtaFieldCalc(DtaFieldProp):
    def __len__(self) -> int:
        return 1

    def __str__(self) -> str:
        return 'calculated: {}'.format(super().__str__())

    def read(self, data: BinaryData) -> List[float]:
        # return value to be added to dataset during read (calculation will follow)
        return [0.0]


@dataclass()
class DtaFieldAnalog(DtaFieldProp):
    """Define field in DTA file for analog values."""
    factor: float = 10.0
    precision: int = 10

    def __len__(self) -> int:
        return 1

    def __str__(self) -> str:
        return 'analog: {} factor={:.1f}'.format(
            super().__str__(),
            self.factor
        )

    def read(self, data: BinaryData) -> List[float]:
        """Read value from DTA file and convert it."""
        val = data.get_int16()
        res = val / self.factor
        return [round(res * self.precision) / self.precision]


@dataclass()
class DtaFieldDigitalBit(DtaFieldProp):
    """Define single bit of digital field."""
    pos: int = 0
    output: bool = False

    def __len__(self) -> int:
        return 1  # pragma: no cover

    def __str__(self) -> str:
        return 'digital_bit: {} direction={}'.format(
            super().__str__(),
            'output' if self.output else 'input'
        )

    def get_bitvalue(self, val: int) -> bool:
        """Extract value of single bit from int."""
        return True if val & (1 << self.pos) else False

    def set_visible(self, val: int):
        """Set visibility flag from int value."""
        self.visible = self.get_bitvalue(val)

    def set_factory_only(self, val: int):
        """Set factory_only flag from int value."""
        self.factory_only = self.get_bitvalue(val)

    def set_input(self, val: int):
        """Set input flag from int value."""
        self.output = self.get_bitvalue(val)


@dataclass()
class DtaFieldDigital(DtaField):
    """Define container for digital fields ind DTA file."""
    bits: List[DtaFieldDigitalBit] = field(default_factory=lambda: [])

    def __len__(self) -> int:
        return len(self.bits)

    def __str__(self) -> str:
        return 'digital_container: {} digital_items={}'.format(
            super().__str__(),
            len(self.bits)
        )

    def __getitem__(self, pos: int) -> DtaFieldDigitalBit:
        return self.bits[pos]

    def read(self, data: BinaryData) -> List[float]:
        """Read data from DTA file."""
        val = data.get_uint16()
        res: List[float] = []
        for i in range(len(self.bits)):
            bit_val = bool(val & (1 << i))
            res.append(1.0 if bit_val else 0.0)
        return res

    def get_names(self) -> List[str]:
        return [bit.name for bit in self.bits]


@dataclass()
class DtaFieldEnum(DtaFieldProp):
    """Define field for ENUM values in DTA file."""
    value_strings: List[str] = field(default_factory=lambda: [])

    def __len__(self) -> int:
        return 1

    def __str__(self) -> str:
        return 'enum: {} items=["{}"]'.format(
            super().__str__(),
            '" "'.join(self.value_strings)
        )

    def read(self, data: BinaryData) -> List[float]:
        return [float(data.get_uint16())]

    # def val2str(self, value) -> str:
    #     return self.value_strings[value]

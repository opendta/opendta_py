# ----------------------------------------------------------------------
# Copyright (C) 2018  opendta@gmx.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

"""Define package installation.

install package for development:
    usage: pip install -e .
"""

# ignore coverage in this file
if True:  # pragma: no cover
    from setuptools import setup, find_packages
    from opendta import pkg_info

    def readme():
        """Read README.rst file for long_description."""
        with open('README.rst') as f:
            return f.read()

    setup(
        name=pkg_info['name'],
        version=pkg_info['version'],
        description=pkg_info['description'],
        long_description=readme(),
        keywords=pkg_info['keywords'],
        url=pkg_info['url'],
        author=pkg_info['author'],
        author_email=pkg_info['author_email'],
        license=pkg_info['license'],
        classifiers=pkg_info['classifiers'],
        packages=find_packages(exclude=['doc', 'tests*']),
        entry_points={
            'console_scripts': [
                # dta2csv script in bin folder
                'dta2csv=opendta.dta2csv:Dta2Csv.main'
            ],
        },
        include_package_data=True
    )

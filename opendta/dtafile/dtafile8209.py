# ----------------------------------------------------------------------
# Copyright (C) 2020  opendta@gmx.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

"""Process DTA file version 8209."""
from dataclasses import dataclass, field
from typing import List

from .binarydata import BinaryData
from .dtafile import DtaFile
from .dtafield import DtaFieldAnalog, DtaFieldDigital, DtaFieldDigitalBit, DtaFieldTimestamp, DtaFieldProp, \
    DtaField, DtaRGB, DtaFieldUnknown, DtaFieldCalc


@dataclass()
class DtaFieldLUT(DtaFieldProp):
    info: dict = field(default_factory=lambda: {})

    def __len__(self) -> int:
        return 1

    def __str__(self) -> str:
        return 'analog_lut: {}'.format(super().__str__())

    def calc_lut_data(self, value: int) -> float:
        """Calculate field value from look-up table."""
        info = self.info
        idx: float = round((value - info['offset']) / info['delta'])
        if idx > len(info['data']) - 2:  # pragma: no cover
            idx = len(info['data']) - 2

        # linear approximation
        x1: float = idx * info['delta'] + info['offset']
        x2: float = (idx + 1) * info['delta'] + info['offset']
        y1: float = info['data'][idx]
        y2: float = info['data'][idx + 1]

        m: float = (y2 - y1) / (x2 - x1)
        n: float = y1 - m * x1

        res = m * value + n
        res = res / info['precision']
        res = round(res * info['precision']) / info['precision']
        return res

    def read(self, data: BinaryData) -> List[float]:
        val = data.get_uint16()
        return [self.calc_lut_data(val)]


class DtaFile8209(DtaFile):
    """Process DTA file version 8209."""

    DATASET_LENGTH: int = 168

    TEMP_CONV_LUT = [
        # LUT for TRL, TVL, TBW, TFB1, TRLext
        {
            'data': [1550, 1550, 1550, 1438, 1305, 1205, 1128,
                     1063, 1007, 959, 916, 878, 843, 811, 783, 756, 732, 708,
                     685, 664, 647, 625, 607, 590, 574, 558, 543, 529, 515, 501,
                     487, 474, 461, 448, 436, 424, 412, 401, 390, 379, 368, 358,
                     348, 338, 328, 318, 308, 299, 289, 279, 269, 260, 250, 241,
                     232, 223, 214, 205, 196, 187, 178, 170, 161, 152, 144,
                     135, 127, 118, 109, 100, 92, 83, 74, 65, 56, 47, 38, 29,
                     20, 11, 1, -7, -17, -26, -37, -48, -58, -68, -78, -90,
                     -102, -112, -124, -137, -150, -162, -175, -190, -205,
                     -220, -237, -255, -273, -279, -279],
            'offset': 0,
            'delta': 10,
            'precision': 10
        },

        # LUT for TWQein, TWQaus, TA
        {
            'data': [1550, 1435, 1133, 971, 862, 781, 718,
                     664, 618, 579, 545, 514, 486, 460, 435, 413, 392, 372, 354,
                     337, 321, 306, 290, 276, 261, 248, 235, 223, 210, 199, 188,
                     177, 166, 156, 146, 136, 127, 117, 108, 99, 90, 82, 73, 65,
                     57, 48, 40, 32, 25, 17, 9, 1, -5, -12, -20, -27, -35, -42,
                     -50, -57, -63, -70, -77, -85, -92, -100, -107, -113, -120,
                     -127, -134, -142, -150, -156, -163, -169, -177, -184, -192,
                     -200, -207, -214, -221, -229, -237, -246, -254, -261, -269,
                     -277, -286, -296, -304, -313, -322, -331, -342, -352, -362,
                     -372, -384, -396, -408, -411, -411],
            'offset': 0,
            'delta': 10,
            'precision': 10
        },

        # LUT for THG
        {
            'data': [1550, 1550, 1550, 1550, 1550, 1550, 1550,
                     1537, 1468, 1409, 1357, 1311, 1268, 1229, 1193, 1160, 1130,
                     1100, 1074, 1048, 1024, 1000, 978, 956, 936, 916, 896, 879,
                     861, 843, 827, 811, 795, 780, 765, 750, 737, 723, 709, 695,
                     681, 668, 655, 646, 630, 617, 605, 593, 581, 570, 558, 547,
                     535, 524, 513, 502, 490, 479, 467, 456, 444, 433, 421, 410,
                     398, 387, 376, 364, 353, 341, 330, 318, 306, 294, 282, 269,
                     256, 243, 230, 217, 203, 189, 175, 161, 146, 131, 116, 99,
                     83, 65, 47, 27, 7, -14, -37, -62, -90, -120, -155, -194,
                     -240, -300, -378, -411, -411],
            'offset': 0,
            'delta': 10,
            'precision': 10
        },

        # LUT for TSS, TSK
        {
            'data': [1550, 1550, 1550, 1550, 1550, 1550, 1550, 1537, 1468, 1409, 1357,
                     1311, 1268, 1229, 1193, 1160, 1130, 1100, 1074, 1048, 1024, 1000,
                     978, 956, 936, 916, 896, 879, 861, 843, 827, 811, 795, 780, 765,
                     750, 737, 723, 709, 695, 681, 668, 655, 646, 630, 617, 605, 593,
                     581, 570, 558, 547, 535, 524, 513, 502, 490, 479, 467, 456, 444,
                     433, 421, 410, 398, 387, 376, 365, 354, 343, 331, 319, 308, 296,
                     283, 270, 257, 244, 231, 218, 204, 191, 177, 162, 148, 133, 117,
                     101, 84, 67, 48, 29, 9, -12, -35, -60, -87, -117, -152, -189,
                     -235, -292, -369, -411, -411],
            'offset': 0,
            'delta': 40,
            'precision': 10
        },

        # LUT for TFB2, TFB3, TEE
        {
            'data': [1550, 1550, 1550, 1438, 1305, 1205, 1128, 1063, 1007, 959, 916, 878,
                     843, 811, 783, 756, 732, 708, 685, 664, 647, 625, 607, 590, 574,
                     558, 543, 529, 515, 501, 487, 474, 461, 448, 436, 424, 412, 401,
                     390, 379, 368, 358, 348, 338, 328, 318, 308, 299, 289, 279, 269,
                     260, 250, 241, 232, 223, 214, 205, 196, 187, 178, 170, 161, 152,
                     144, 135, 127, 119, 110, 101, 93, 84, 75, 66, 57, 48, 39, 30, 21,
                     12, 2, -7, -16, -25, -36, -47, -57, -66, -77, -89, -101, -111,
                     -123, -135, -149, -161, -174, -189, -204, -219, -235, -254, -271,
                     -279, -279],
            'offset': 0,
            'delta': 40,
            'precision': 10
        },
    ]

    FIELDS: List[DtaField] = [
        # TR_DE: Datum und Uhrzeit in Sekunden von 1.1.1970 (Unixzeit)
        # TR_EN: date and time in seconds from 1970-01-01 (unix time)
        # [0  :3  ]
        DtaFieldTimestamp(name='Zeitstempel'),
        # [4  :7  ]
        DtaFieldUnknown(length=4),
        # [8  :9  ] StatusA = Status der Ausgaenge / state ouf outputs
        DtaFieldDigital(category='Heizkreis', bits=[
            # TR_DE: HUP = Heizungsumwaelzpumpe
            # TR_EN: HCP  = heat circulation pump
            DtaFieldDigitalBit(category='Heizkreis', name='HUP', pos=0, color=DtaRGB(255, 0, 0), output=True),
            # TR_DE: ZUP  = Zusatzumwaelzpumpe
            # TR_EN: ACP  = additional heat circulation pump
            DtaFieldDigitalBit(category='Heizkreis', name='ZUP', pos=1, color=DtaRGB(255, 65, 0), output=True),
            # TR_DE: BUP  = Brauswarmwasserumwaelzpumpe oder Drei-Wege-Ventil auf Brauchwassererwaermung
            # TR_EN: DHW  = domestic hot warter
            DtaFieldDigitalBit(category='Heizkreis', name='BUP', pos=2, color=DtaRGB(128, 0, 128), output=True),
            # TR_DE: ZW2  = Zusaetzlicher Waermeerzeuger 2 / Sammelstoerung
            # TR_EN: SHG2 = second heat generator 2
            DtaFieldDigitalBit(category='Heizkreis', name='ZW2', pos=3, color=DtaRGB(0, 0, 255), output=True),
            # TR_DE: MA1  = Mischer 1 auf
            # TR_EN: MO1  = mixer 1 open
            DtaFieldDigitalBit(category='Heizkreis', name='MA1', pos=4, color=DtaRGB(128, 128, 128), output=True),
            # TR_DE: MZ1  = Mischer 1 zu
            # TR_EN: MC1  = mixer 1 close
            DtaFieldDigitalBit(category='Heizkreis', name='MZ1', pos=5, color=DtaRGB(160, 160, 160), output=True),
            # TR_DE: ZIP  = Zirkulationspumpe
            # TR_EN: HWP  = domestic hot warter circulation pump
            DtaFieldDigitalBit(category='Heizkreis', name='ZIP', pos=6, color=DtaRGB(160, 0, 160), output=True),
            # TR_DE: VD1  = Verdichter 1
            # TR_EN: CP1  = compressor 1
            DtaFieldDigitalBit(category='Heizkreis', name='VD1', pos=7, color=DtaRGB(0, 128, 128), output=True),
            # TR_DE: VD2  = Verdichter 2
            # TR_EN: CP2  = compressor 2
            DtaFieldDigitalBit(category='Heizkreis', name='VD2', pos=8, color=DtaRGB(0, 192, 192), output=True),
            # TR_DE: VENT = Ventilation des WP Gehaeses / 2. Stufe des Ventilators
            # TR_EN: VENT = ventilation of HP chassis / 2nd stage of fan
            DtaFieldDigitalBit(category='Heizkreis', name='VENT', pos=9, color=DtaRGB(0, 255, 0), output=True),
            # TR_DE: AV   = Abtauventil (Kreislaufumkehr)
            # TR_EN: DV   = defrosting valve / circuit inversion
            DtaFieldDigitalBit(category='Heizkreis', name='AV', pos=10, color=DtaRGB(255, 0, 255), output=True),
            # TR_DE: VBS  = Ventilator, Brunnen- oder Soleumwaelzpumpe
            # TR_DE: VWB  = fan, well- or brine-circulation-pump
            DtaFieldDigitalBit(category='Heizkreis', name='VBS', pos=11, color=DtaRGB(0, 128, 0), output=True),
            # TR_DE: ZW1  = Zusaetzlicher Waermeerzeuger 1
            # TR_EN: SHG2 = second heat generator 1
            DtaFieldDigitalBit(category='Heizkreis', name='ZW1', pos=12, color=DtaRGB(128, 0, 0), output=True),
        ]),
        # [10 :43 ]
        DtaFieldUnknown(length=34),
        # [44 :45 ] StatusE = Status der Eingaenge (die Bits sind invertiert zur Funktion)
        #                     state of inputs (bits are negated to function)
        DtaFieldDigital(category='Heizkreis', bits=[
            # TR_DE: HD   = Hochdruckpressostat
            # TR_EN: HP   = high-presure pressostat
            DtaFieldDigitalBit(category='Heizkreis', name='HD', pos=0, color=DtaRGB(255, 0, 0), output=False),
            # TR_DE: ND   = Niederdruckpressostat
            # TR_EN: LP   = low-pressure pressostat
            DtaFieldDigitalBit(category='Heizkreis', name='ND', pos=1, color=DtaRGB(0, 0, 255), output=False),
            # TR_DE: MOT  = Motorschutz
            # TR_DE: MOT  = motor protection
            DtaFieldDigitalBit(category='Heizkreis', name='MOT', pos=2, color=DtaRGB(0, 128, 0), output=False),
            # TR_DE: ASD  = Abtau/Soledruck/Durchfluss
            # TR_DE: DBF  = defrost, brine pressure, flow
            DtaFieldDigitalBit(category='Heizkreis', name='ASD', pos=3, color=DtaRGB(0, 0, 128), output=False),
            # TR_DE: EVU  = EVU Sperre
            # TR_DE: ES   = electrical supply
            DtaFieldDigitalBit(category='', name='EVU', pos=4, color=DtaRGB(128, 128, 0), output=False),
        ]),
        # [46 :51 ]
        DtaFieldUnknown(length=6),
        # TR_DE: TFB1    = Temperatur Fussbodenheizung 1
        # TR_EN: TFH1    = temperature floor heating 1
        # [52 :53 ]
        DtaFieldLUT(category='Heizkreis', name='TFB1', color=DtaRGB(0, 0, 0), info=TEMP_CONV_LUT[0]),
        # TR_DE: TBW     = Temperatur Brauch-Warm-Wasser
        # TR_EN: TDHW    = temperature domestic hot warter
        # [54 :55 ]
        DtaFieldLUT(category='Heizkreis', name='TBW', color=DtaRGB(128, 0, 128), info=TEMP_CONV_LUT[0]),
        # TR_DE: TA      = Aussentemperatur
        # TR_EN: TA      = ambient (external) temperature
        # [56 :57 ]
        DtaFieldLUT(category='Heizkreis', name='TA', color=DtaRGB(128, 128, 0), info=TEMP_CONV_LUT[1]),
        # TR_DE: TRLext  = Temperatur Heizung Ruecklauf extern
        # TR_EN: TRFext  = return flow temperature in separate tank
        # [58 :59 ]
        DtaFieldLUT(category='Heizkreis', name='TRLext', color=DtaRGB(0, 128, 128), info=TEMP_CONV_LUT[0]),
        # TR_DE: TRL     = Temperatur Heizung Ruecklauf
        # TR_EN: TRF     = return flow temperature of heat circuit
        # [60 :61 ]
        DtaFieldLUT(category='Heizkreis', name='TRL', color=DtaRGB(0, 0, 255), info=TEMP_CONV_LUT[0]),
        # TR_DE: TVL     = Temperatur Heizung Vorlauf
        # TR_EN: TIF     = inflow temperature heating circle
        # [62 :63 ]
        DtaFieldLUT(category='Heizkreis', name='TVL', color=DtaRGB(255, 0, 0), info=TEMP_CONV_LUT[0]),
        # TR_DE: THG     = Temperatur Heissgas
        # TR_EN: THG     = hot gas temperature
        # [64 :65 ]
        DtaFieldLUT(category='Kaeltekreis', name='THG', color=DtaRGB(128, 0, 0), info=TEMP_CONV_LUT[2]),
        # TR_DE: TWQaus  = Temperatur Waermequelle Austritt
        # TR_EN: THSout  = heat source outlet temperature
        # [66 :67 ]
        DtaFieldLUT(category='Quelle', name='TWQaus', color=DtaRGB(0, 0, 128), info=TEMP_CONV_LUT[1]),
        # [68 :69 ]
        DtaFieldUnknown(length=2),
        # TR_DE: TWQein  = Temperatur Waermequelle Eintritt
        # TR_EN: THSin   = heat source inlet temperature
        # [70 :71 ]
        DtaFieldLUT(category='Quelle', name='TWQein', color=DtaRGB(0, 128, 0), info=TEMP_CONV_LUT[1]),
        # [72 :79 ]
        DtaFieldUnknown(length=8),
        # TR_DE: TRLsoll = Solltemperatur Heizung Ruecklauf
        # TR_EN: TRFtrgt = heating circle return flow setpoint
        # [80 :81 ]
        DtaFieldAnalog(category='Heizkreis', name='TRLsoll', color=DtaRGB(255, 0, 255), factor=10.0),
        # [82 :83 ] TRLsoll_highbytes = zwei Extra-Byte fuer TRLsoll (werden nicht ausgelesen)
        DtaFieldUnknown(length=2),
        # TR_DE: TMK1soll = Solltemperatur Mischer Kreis 1
        # TR_EN: TM1trgt  = mixer 1 temperature setpoint
        # [84 :85 ]
        DtaFieldAnalog(category='Heizkreis', name='TMK1soll', color=DtaRGB(128, 128, 128), factor=10.0),
        # [86 :87 ] TMK1soll_highbytes = zwei Extra-Byte fuer TMK1soll (werden nicht ausgelesen)
        DtaFieldUnknown(length=2),
        # [88 :127]
        DtaFieldUnknown(length=40),
        # [128:129] ComfortPlatine: Indikator, ob und welche Erweiterung eingebaut ist
        DtaFieldAnalog(category='ComfortPlatine', name='CfrtPlat', color=DtaRGB(0, 0, 0), factor=1.0, precision=1),
        # [130:131]
        DtaFieldUnknown(length=2),
        # [132:133] StatusA_CP = Status der Ausgaenge der ComfortPlatine / state of comfort board
        DtaFieldDigital(category='ComfortPlatine', bits=[
            # TR_DE: AI1DIV = Spannungsteiler an AI1: wann AI1DIV dann AI1 = AI1/2
            # TR_EN: AI1DIV = voltage devieder at AI1: if AI1DIV then AI1 = AI1/2
            # - bit 6
            DtaFieldDigitalBit(category='ComfortPlatine', name='AI1DIV', pos=6, color=DtaRGB(255, 255, 255), output=True),
            # TR_DE: SUP  = Schwimmbadumwaelzpumpe
            # TR_DE: PCP  = pool circulation pump
            # - bit 7
            DtaFieldDigitalBit(category='ComfortPlatine', name='SUP', pos=7, color=DtaRGB(255, 0, 255), output=True),
            # TR_DE: FUP2 = Mischkreispumpe 2 / Kuehlsignal 2
            # TR_EN: MCP2 = mixer 2 circulation pump
            # - bit 8
            DtaFieldDigitalBit(category='ComfortPlatine', name='FUP2', pos=8, color=DtaRGB(0, 255, 0), output=True),
            # TR_DE: MA2  = Mischer 2 auf
            # TR_EN: MO2  = mixer 2 open
            # - bit 9
            DtaFieldDigitalBit(category='ComfortPlatine', name='MA2', pos=9, color=DtaRGB(0, 128, 0), output=True),
            # TR_DE: MZ2  = Mischer 2 zu
            # TR_EN: MC2  = mixer 2 close
            # - bit 10
            DtaFieldDigitalBit(category='ComfortPlatine', name='MZ2', pos=10, color=DtaRGB(0, 160, 0), output=True),
            # TR_DE: MA3  = Mischer 3 auf
            # TR_EN: MO3  = mixer 3 open
            # - bit 11
            DtaFieldDigitalBit(category='ComfortPlatine', name='MA3', pos=11, color=DtaRGB(0, 0, 238), output=True),
            # TR_DE: MZ3  = Mischer 3 zu
            # TR_EN: MC3  = mixer 3 close
            # - bit 12
            DtaFieldDigitalBit(category='ComfortPlatine', name='MZ3', pos=12, color=DtaRGB(0, 0, 160), output=True),
            # TR_DE: FUP3 = Mischkreispumpe 3 / Kuehlsignal 3
            # TR_EN: MCP3 = mixer 3 circulation pump
            # - bit 13
            DtaFieldDigitalBit(category='ComfortPlatine', name='FUP3', pos=13, color=DtaRGB(0, 0, 255), output=True),
            # TR_DE: ZW3 = Zusaetzlicher Waermeerzeuger 3
            # TR_EN: SHG3 = second heat generator 3
            # - bit 14
            DtaFieldDigitalBit(category='ComfortPlatine', name='ZW3', pos=14, color=DtaRGB(0, 0, 192), output=True),
            # TR_DE: SLP = Solarladepumpe
            # TR_EN: SCP = solar circuit pump
            # - bit 15
            DtaFieldDigitalBit(category='ComfortPlatine', name='SLP', pos=15, color=DtaRGB(255, 128, 0), output=True),
        ]),
        # [134:135]
        DtaFieldUnknown(length=2),
        # TR_DE: AO1  = ComfortPlatine: Analoger Ausgang 1
        # TR_EN: AO1  = comfort board: analoger output 1
        # [136:137]
        DtaFieldAnalog(category='ComfortPlatine', name='AO1', color=DtaRGB(0, 0, 255), factor=381.8, precision=100),
        # TR_DE: AO2  = ComfortPlatine: Analoger Ausgang 2
        # TR_EN: AO2  = comfort board: analoger output 2
        # [138:139]
        DtaFieldAnalog(category='ComfortPlatine', name='AO2', color=DtaRGB(0, 128, 0), factor=381.8, precision=100),
        # [140:141] StatusE_CP = Status der Eingaenge der ComfortPlatine / input states of comfort board
        DtaFieldDigital(category='ComfortPlatine', bits=[
            # TR_DE: SWT  = Schwimmbadthermostat
            # TR_DE: PT   = pool thermostat
            # - bit 4
            DtaFieldDigitalBit(category='ComfortPlatine', name='SWT', pos=4, color=DtaRGB(255, 0, 255), output=False),
        ]),
        # [142:143]
        DtaFieldUnknown(length=2),
        # TR_DE: TSS  = Temperatur Solar Speicher
        # TR_EN: TST  = temperature solar tank
        # [144:145]
        DtaFieldLUT(category='ComfortPlatine', name='TSS', color=DtaRGB(255, 128, 0), info=TEMP_CONV_LUT[3]),
        # TR_DE: TSK  = Temperatur Solar Kollektor
        # TR_EN: TSC  = temperature solar collector
        # [146:147]
        DtaFieldLUT(category='ComfortPlatine', name='TSK', color=DtaRGB(255, 64, 0), info=TEMP_CONV_LUT[3]),
        # TR_DE: TFB2 = Temperatur Fussbodenheizung 2
        # TR_EN: TFH2 = temperature floor heating 2
        # [148:149]
        DtaFieldLUT(category='ComfortPlatine', name='TFB2', color=DtaRGB(0, 0, 0), info=TEMP_CONV_LUT[4]),
        # TR_DE: TFB3 = Temperatur Fussbodenheizung 3
        # TR_EN: TFH2 = temperature floor heating 3
        # [150:151]
        DtaFieldLUT(category='ComfortPlatine', name='TFB3', color=DtaRGB(0, 0, 0), info=TEMP_CONV_LUT[4]),
        # TR_DE: TEE  = Temperatur Externe Energiequelle
        # TR_EN: TEE  = temperature external energy source
        # [152:153]
        DtaFieldLUT(category='ComfortPlatine', name='TEE', color=DtaRGB(0, 0, 0), info=TEMP_CONV_LUT[4]),
        # [154:157]
        DtaFieldUnknown(length=4),
        # TR_DE:  AI1 = ComfortPlatine: Analoger Eingang 1
        # TR_EN:  AI1 = comfort board: analoger input 1
        # [158:159]
        DtaFieldAnalog(category='ComfortPlatine', name='AI1', color=DtaRGB(255, 0, 0), factor=275.4, precision=100),
        # TR_DE: TMK2soll = Solltemperatur Mischer Kreis 2
        # TR_EN: TM2trgt  = mixer 2 temperature setpoint
        # [160:161]
        DtaFieldAnalog(category='ComfortPlatine', name='TMK2soll', color=DtaRGB(128, 128, 128), factor=10.0),
        # [162:163] TMK2soll_highbytes = zwei Extra-Byte fuer TMK2soll (werden nicht ausgelesen)
        DtaFieldUnknown(length=2),
        # TR_DE: TMK3soll = Solltemperatur Mischer Kreis 3
        # TR_EN: TM3trgt  = mixer 3 temperature setpoint
        # [164:165]
        DtaFieldAnalog(category='ComfortPlatine', name='TMK3soll', color=DtaRGB(128, 128, 128), factor=10.0),
        # [166:167] TMK3soll_highbytes = zwei Extra-Byte fuer TMK3soll (werden nicht ausgelesen)
        DtaFieldUnknown(length=2),

        # calculated fields
        # TR_DE: DF = Durchfluss (berechnet aus AI1)
        # TR_EN: F = flow in heat circuit (calculated from AI1)
        DtaFieldCalc(category='Berechnet', name='DF', color=DtaRGB(0, 128, 0)),
        # TR_DE: SpHz = Spreizung Heizkreis (TVL - TRL)
        # TR_EN: SpHt = spread of heat circuit (TI - TO)
        DtaFieldCalc(category='Berechnet', name='SpHz', color=DtaRGB(128, 0, 0)),
        # TR_DE: SpWq = Spreizung Wärmequelle (TWQein - TWQaus)
        # TR_NE: SpSrc = spread of source circuit (THI - THO)
        DtaFieldCalc(category='Berechnet', name='SpWq', color=DtaRGB(0, 0, 128)),
        # TR_DE: Qth = thermische Leistung (berechnet aus Durchfluss und Spreizung Heizkreis)
        # TR_EN: Qth = thermal power (calculated from flow and spread in heat circuit)
        DtaFieldCalc(category='Berechnet', name='Qth', color=DtaRGB(0, 0, 0)),
    ]

    def __init__(self, data: bytes = b''):
        super().__init__(data)

        self._fields = self.FIELDS.copy()
        self._dataset_length = self.DATASET_LENGTH

        if self.version == 8208:
            self._fields.append(
                    # [168:188] unknown extra data
                    DtaFieldUnknown(length=20)
            )
            self._dataset_length += 20

        # read header
        if self._dta.bytes_to_read >= 4:
            # self._subversion = self.get_uint32()
            self._dta.unused_data(4)

        # read datasets
        while True:
            ds = self.read_dataset()
            if ds is None:
                break
            self._data.append(ds)
        self._calc_values()

        # sort datasets (by date)
        self._data = sorted(self._data, key=lambda x: x[0])

        # clean up
        del self._dta

    def _calc_values(self):
        """Calculate additional dataset fields."""
        pos_ai1 = self.field_pos('AI1')
        pos_df = self.field_pos('DF')
        pos_tvl = self.field_pos('TVL')
        pos_trl = self.field_pos('TRL')
        pos_sphz = self.field_pos('SpHz')
        pos_twqein = self.field_pos('TWQein')
        pos_twqaus = self.field_pos('TWQaus')
        pos_spwq = self.field_pos('SpWq')
        pos_qth = self.field_pos('Qth')

        for ds in self._data:
            flow = self.calc_flow_rate_grundfoss_vfs5_5_100(ds[pos_ai1])
            ds[pos_df] = flow
            sp_heat = round((ds[pos_tvl] - ds[pos_trl]) * 10) / 10.0
            ds[pos_sphz] = sp_heat
            ds[pos_spwq] = round((ds[pos_twqein] - ds[pos_twqaus]) * 10) / 10.0
            ds[pos_qth] = self.calc_qth(flow, sp_heat)

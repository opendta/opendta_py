# ----------------------------------------------------------------------
# Copyright (C) 2018  opendta@gmx.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

"""Load DtaFile depending on version."""

from .dtafile import DtaFile, DtaViolation
from .dtafile8208 import DtaFile8208
from .dtafile8209 import DtaFile8209
from .dtafile9001 import DtaFile9001
from .dtafile9003 import DtaFile9003
from typing import BinaryIO
from gettext import gettext as _


def dta_load_from_bytes(data: bytes = b'') -> DtaFile:
    """Create DtaFile instance from bytes array according to version number."""
    # return empty object
    if data == b'':
        return DtaFile(b'')

    # get version and return instance accordingly
    version = DtaFile.get_version(data)
    if version == 8208:
        return DtaFile8208(data)
    elif version == 8209:
        return DtaFile8209(data)
    elif version == 9001:
        return DtaFile9001(data)
    elif version == 9003:
        return DtaFile9003(data)
    else:
        raise DtaViolation(_('DTA version {version:d} (0x{version:x}) not supported!').format(version=version))


def dta_load_from_io(file_io: BinaryIO) -> DtaFile:
    """Create DtaFile instance from IO object."""
    return dta_load_from_bytes(file_io.read())


def dta_load_from_file(file_name: str) -> DtaFile:
    """Create DtaFile instance from file name."""
    with open(file_name, 'rb') as f:
        return dta_load_from_io(f)

# ------------------------------------------------------------------------------
#  Copyright (C) 2020  opendta@gmx.de
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

import struct
from typing import Tuple


class BinaryData(object):
    """Handle access to binary data."""

    def __init__(self, data: bytes = b''):
        """Define constructor."""
        self._data: bytes = data
        self._data_len: int = len(data)
        self._pos: int = 0

    @property
    def pos(self) -> int:
        """Get current position in file."""
        return self._pos

    @pos.setter
    def pos(self, pos):
        """Set position to new value."""
        self._pos = pos

    @property
    def bytes_to_read(self) -> int:
        """Get amount of bytes available in DTA stream."""
        return self._data_len - self._pos

    # def get_data(self, size: int) -> bytes:
    #     """Get count bytes from DTA file."""
    #     res = self._data[self._pos:self._pos + size]
    #     self._pos += size
    #     return res

    def _unpack(self, fmt: str, size: int) -> int:
        """Convert binary data into requested format."""
        # assert self.bytes_to_read >= size, 'Unexpected end of file!'
        res: int = struct.unpack('<' + fmt, self._data[self._pos:self._pos + size])[0]
        self._pos += size
        return res

    def get_uint32(self) -> int:
        """Extract unsigned signed int."""
        return self._unpack('I', 4)

    def get_uint16(self) -> int:
        """Extract unsigned signed short."""
        return self._unpack('H', 2)

    def get_int16(self) -> int:
        """Extract signed short."""
        return self._unpack('h', 2)

    def get_byte(self) -> int:
        """Extract byte."""
        return self._unpack('B', 1)

    def get_string(self) -> str:
        """Extract string."""
        res: str = ''
        while True:
            c: int = self.get_byte()
            if c == 0:
                break
            res += chr(c)
        return res

    def unused_data(self, size: int):
        """Skip unused data."""
        self._pos += size

    def get_rgb(self) -> Tuple[int, int, int]:
        """Read three bytes (red, green, blue)."""
        r = self.get_byte()
        g = self.get_byte()
        b = self.get_byte()
        return r, g, b

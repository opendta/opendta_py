# ----------------------------------------------------------------------
# Copyright (C) 2018  opendta@gmx.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

"""Process DTA file version 9001."""
from typing import List, Optional

from .dtafield import DtaField, DtaFieldAnalog, DtaFieldCalc, DtaFieldDigital, DtaFieldDigitalBit, \
    DtaFieldUnknown, DtaFieldTimestamp, DtaRGB
from .dtafile import DtaFile, TDTADataSet, DtaViolation


class DtaFile9001(DtaFile):
    """Process DTA file version 9001."""

    FIELDS_0: List[DtaField] = [
        # TR_DE: Datum und Uhrzeit in Sekunden von 1.1.1970 (Unixzeit)
        # TR_EN: date and time in seconds from 1970-01-01 (unix time)
        # [0 :3 ] Datum und Uhrzeit in Sekunden von 1.1.1970 (Unixzeit)
        DtaFieldTimestamp(name='Zeitstempel'),
        # TR_DE: TVL     = Temperatur Heizung Vorlauf
        # TR_EN: TIF     = inflow temperature heating circle
        # [4 :5 ] TVL
        DtaFieldAnalog(category='WP', name='TVL', color=DtaRGB(255, 0, 0), factor=10.0),
        # TR_DE: TRL     = Temperatur Heizung Ruecklauf
        # TR_EN: TRF     = return flow temperature of heat circuit
        # [6 :7 ] TRL
        DtaFieldAnalog(category='WP', name='TRL', color=DtaRGB(0, 0, 255), factor=10.0),
        # TR_DE: TWQein  = Temperatur Waermequelle Eintritt
        # TR_EN: THSin   = heat source inlet temperature
        # [8 :9 ] TWQein
        DtaFieldAnalog(category='WP', name='TWQein', color=DtaRGB(0, 128, 0), factor=10.0),
        # TR_DE: TWQaus  = Temperatur Waermequelle Austritt
        # TR_EN: THSout  = heat source outlet temperature
        # [10:11] TWQaus
        DtaFieldAnalog(category='WP', name='TWQaus', color=DtaRGB(0, 0, 128), factor=10.0),
        # TR_DE: THG     = Temperatur Heissgas
        # TR_EN: THG     = hot gas temperature
        # [12:13] THG
        DtaFieldAnalog(category='WP', name='THG', color=DtaRGB(128, 0, 0), factor=10.0),
        # TR_DE: TBW     = Temperatur Brauch-Warm-Wasser
        # TR_EN: TDHW    = temperature domestic hot warter
        # [14:15] TBW
        DtaFieldAnalog(category='WP', name='TBW', color=DtaRGB(128, 0, 128), factor=10.0),
        # TR_DE: TFB1    = Temperatur Fussbodenheizung 1
        # TR_EN: TFH1    = temperature floor heating 1
        # [16:17] TFB1
        DtaFieldAnalog(category='WP', name='TFB1', color=DtaRGB(0, 0, 0), factor=10.0),
        # TR_DE: TA      = Aussentemperatur
        # TR_EN: TA      = ambient (external) temperature
        # [18:19] TA
        DtaFieldAnalog(category='WP', name='TA', color=DtaRGB(128, 128, 0), factor=10.0),
        # TR_DE: TRLext  = Temperatur Heizung Ruecklauf extern
        # TR_EN: TRFext  = return flow temperature in separate tank
        # [20:21] TRLext
        DtaFieldAnalog(category='WP', name='TRLext', color=DtaRGB(0, 128, 128), factor=10.0),
        # TR_DE: TRLsoll = Solltemperatur Heizung Ruecklauf
        # TR_EN: TRFtrgt = heating circle return flow setpoint
        # [22:23] TRLsoll
        DtaFieldAnalog(category='WP', name='TRLsoll', color=DtaRGB(255, 0, 255), factor=10.0),
        # TR_DE: TMK1soll = Solltemperatur Mischer Kreis 1
        # TR_EN: TM1trgt  = mixer 1 temperature setpoint
        # [24:25] TMK1soll
        DtaFieldAnalog(category='WP', name='TMK1soll', color=DtaRGB(128, 128, 128), factor=10.0),
        # [26:27] StatusE = Status der Eingaenge (die Bits sind invertiert zur Funktion)
        #                   state of inputs (bits are negated to function)
        DtaFieldDigital(category='WP', bits=[
            # TR_DE: HD   = Hochdruckpressostat
            # TR_EN: HP   = high-presure pressostat
            # - bit 0:  HD_  = Hochdruckpressostat
            DtaFieldDigitalBit(category='WP', name='HD', pos=0, color=DtaRGB(255, 0, 0), output=False),
            # TR_DE: ND   = Niederdruckpressostat
            # TR_EN: LP   = low-pressure pressostat
            # - bit 1:  ND_  = Niederdruckpressostat
            DtaFieldDigitalBit(category='WP', name='ND', pos=1, color=DtaRGB(0, 0, 255), output=False),
            # TR_DE: MOT  = Motorschutz
            # TR_DE: MOT  = motor protection
            # - bit 2:  MOT_ = Motorschutz
            DtaFieldDigitalBit(category='WP', name='MOT', pos=2, color=DtaRGB(0, 128, 0), output=False),
            # TR_DE: ASD  = Abtau/Soledruck/Durchfluss
            # TR_DE: DBF  = fefrost, brine pressure, flow
            # - bit 3:  ASD_ = Abtau/Soledruck/Durchfluss
            DtaFieldDigitalBit(category='WP', name='ASD', pos=3, color=DtaRGB(0, 0, 128), output=False),
            # TR_DE: EVU  = EVU Sperre
            # TR_DE: ES   = electrical supply
            # - bit 4:  EVU  = EVU Sperre
            DtaFieldDigitalBit(category='WP', name='EVU', pos=4, color=DtaRGB(128, 128, 0), output=False),
        ]),
        # [28:29]   StatusA = Status der Ausgaenge / state ouf outputs
        DtaFieldDigital(category='WP', bits=[
            # TR_DE: HUP = Heizungsumwaelzpumpe
            # TR_EN: HCP  = heat circulation pump
            DtaFieldDigitalBit(category='WP', name='HUP', pos=0, color=DtaRGB(255, 0, 0), output=True),
            # TR_DE: ZUP  = Zusatzumwaelzpumpe
            # TR_EN: ACP  = additional heat circulation pump
            DtaFieldDigitalBit(category='WP', name='ZUP', pos=1, color=DtaRGB(255, 65, 0), output=True),
            # TR_DE: BUP  = Brauswarmwasserumwaelzpumpe oder Drei-Wege-Ventil auf Brauchwassererwaermung
            # TR_EN: DHW  = domestic hot warter
            DtaFieldDigitalBit(category='WP', name='BUP', pos=2, color=DtaRGB(128, 0, 128), output=True),
            # TR_DE: ZW2  = Zusaetzlicher Waermeerzeuger 2 / Sammelstoerung
            # TR_EN: SHG2 = second heat generator 2
            DtaFieldDigitalBit(category='WP', name='ZW2', pos=3, color=DtaRGB(0, 0, 255), output=True),
            # TR_DE: MA1  = Mischer 1 auf
            # TR_EN: MO1  = mixer 1 open
            DtaFieldDigitalBit(category='WP', name='MA1', pos=4, color=DtaRGB(128, 128, 128), output=True),
            # TR_DE: MZ1  = Mischer 1 zu
            # TR_EN: MC1  = mixer 1 close
            DtaFieldDigitalBit(category='WP', name='MZ1', pos=5, color=DtaRGB(160, 160, 160), output=True),
            # TR_DE: ZIP  = Zirkulationspumpe
            # TR_EN: HWP  = domestic hot warter circulation pump
            DtaFieldDigitalBit(category='WP', name='ZIP', pos=6, color=DtaRGB(160, 0, 160), output=True),
            # TR_DE: VD1  = Verdichter 1
            # TR_EN: CP1  = compressor 1
            DtaFieldDigitalBit(category='WP', name='VD1', pos=7, color=DtaRGB(0, 128, 128), output=True),
            # TR_DE: VD2  = Verdichter 2
            # TR_EN: CP2  = compressor 2
            DtaFieldDigitalBit(category='WP', name='VD2', pos=8, color=DtaRGB(0, 192, 192), output=True),
            # TR_DE: VENT = Ventilation des WP Gehaeses / 2. Stufe des Ventilators
            # TR_EN: VENT = ventilation of HP chassis / 2nd stage of fan
            DtaFieldDigitalBit(category='WP', name='VENT', pos=9, color=DtaRGB(0, 255, 0), output=True),
            # TR_DE: AV   = Abtauventil (Kreislaufumkehr)
            # TR_EN: DV   = defrosting valve / circuit inversion
            DtaFieldDigitalBit(category='WP', name='AV', pos=10, color=DtaRGB(255, 0, 255), output=True),
            # TR_DE: VBS  = Ventilator, Brunnen- oder Soleumwaelzpumpe
            # TR_DE: VWB  = fan, well- or brine-circulation-pump
            DtaFieldDigitalBit(category='WP', name='VBS', pos=11, color=DtaRGB(0, 128, 0), output=True),
            # TR_DE: ZW1  = Zusaetzlicher Waermeerzeuger 1
            # TR_EN: SHG2 = second heat generator 1
            DtaFieldDigitalBit(category='WP', name='ZW1', pos=12, color=DtaRGB(128, 0, 0), output=True),
        ]),
        # [30:31]
        DtaFieldUnknown(length=2),
        # TR_DE: TSS  = Temperatur Solar Speicher
        # TR_EN: TST  = temperature solar tank
        # [32:33] TSS
        DtaFieldAnalog(category='WP', name='TSS', color=DtaRGB(255, 128, 0), factor=10.0),
        # TR_DE: TSK  = Temperatur Solar Kollektor
        # TR_EN: TSC  = temperature solar collector
        # [34:35] TSK
        DtaFieldAnalog(category='WP', name='TSK', color=DtaRGB(255, 64, 0), factor=10.0),
        # TR_DE: TFB2 = Temperatur Fussbodenheizung 2
        # TR_EN: TFH2 = temperature floor heating 2
        # [36:37] TFB2
        DtaFieldAnalog(category='WP', name='TFB2', color=DtaRGB(0, 0, 0), factor=10.0),
        # TR_DE: TFB3 = Temperatur Fussbodenheizung 3
        # TR_EN: TFH2 = temperature floor heating 3
        # [38:39] TFB3
        DtaFieldAnalog(category='WP', name='TFB3', color=DtaRGB(0, 0, 0), factor=10.0),
        # TR_DE: TEE  = Temperatur Externe Energiequelle
        # TR_EN: TEE  = temperature external energy source
        # [40:41] TEE
        DtaFieldAnalog(category='WP', name='TEE', color=DtaRGB(0, 0, 0), factor=10.0),
        # [42:45]
        DtaFieldUnknown(length=4),
        # TR_DE: TMK2soll = Solltemperatur Mischer Kreis 2
        # TR_EN: TM2trgt  = mixer 2 temperature setpoint
        # [46:47] TMK2soll
        DtaFieldAnalog(category='WP', name='TMK2soll', color=DtaRGB(128, 128, 128), factor=10.0),
        # TR_DE: TMK3soll = Solltemperatur Mischer Kreis 3
        # TR_EN: TM3trgt  = mixer 3 temperature setpoint
        # [48:49] TMK3soll
        DtaFieldAnalog(category='WP', name='TMK3soll', color=DtaRGB(128, 128, 128), factor=10.0),
        # TR_DE:  AI1 = ComfortPlatine: Analoger Eingang 1
        # TR_EN:  AI1 = comfort board: analoger input 1
        # [50:51] AI
        DtaFieldAnalog(category='WP', name='AI1', color=DtaRGB(255, 0, 0), factor=1000.0, precision=1000),
        # TR_DE: AO1  = ComfortPlatine: Analoger Ausgang 1
        # TR_EN: AO1  = comfort board: analoger output 1
        # [52:53] AO1
        DtaFieldAnalog(category='WP', name='AO1', color=DtaRGB(0, 0, 255), factor=1000.0, precision=1000),
    ]

    FIELDS_123: List[DtaField] = [
        # TR_DE: AO2  = ComfortPlatine: Analoger Ausgang 2
        # TR_EN: AO2  = comfort board: analoger output 2
        # [54:55] AO2
        DtaFieldAnalog(category='WP', name='AO2', color=DtaRGB(0, 128, 0), factor=1000.0, precision=1000),
        # [56:57]
        DtaFieldUnknown(length=2),
        # TR_DE: Asg.VDi  = Ansaug Verdichter
        # TR_EN: Asp.CP   = aspiration compressor
        # [58:59] Ansaug Verdichter
        DtaFieldAnalog(category='WP', name='Asg.VDi', color=DtaRGB(0, 0, 192), factor=10.0),
        # TR_DE: Asg.VDa  = Ansaug Verdampfer
        # TR_EN: Asp.Va   = aspiration vaporiser
        # [60:61] Ansaug Verdampfer
        DtaFieldAnalog(category='WP', name='Asg.VDa', color=DtaRGB(0, 0, 192), factor=10.0),
        # TR_DE: VDHz     = VD Heizung
        # TR_EN: CPHc     = CP heating circuit
        # [62:63] VD Heizung
        DtaFieldAnalog(category='WP', name='VDHz', color=DtaRGB(0, 0, 192), factor=10.0),
        # [64:71]
        DtaFieldUnknown(length=8),
    ]

    FIELDS_13: List[DtaField] = [
        # [72:73]
        DtaFieldUnknown(length=2),
        # TR_DE: UeHz  = Ueberhitzung
        # TR_EN: OH    = overheating
        # [74:75] Ueberhitzung
        DtaFieldAnalog(category='WP', name='UeHz', color=DtaRGB(0, 0, 0), factor=10.0),
        # TR_DE: UeHzsoll = Ueberhitzung Sollwert
        # TR_EN: OHtrgt   = overheating setpoint
        # [76:77] Ueberhiztung Sollwert
        DtaFieldAnalog(category='WP', name='UeHzsoll', color=DtaRGB(0, 0, 0), factor=10.0),
        # [78:79]
        DtaFieldUnknown(length=2),
    ]

    FIELDS_3: List[DtaField] = [
        # [80:97]
        DtaFieldUnknown(length=18),
    ]

    FIELDS_CALC: List[DtaField] = [
        # calculated fields
        # TR_DE: DF = Durchfluss (berechnet aus AI1)
        # TR_EN: F = flow in heat circuit (calculated from AI1)
        DtaFieldCalc(category='Berechnet', name='DF', color=DtaRGB(0, 128, 0)),
        # TR_DE: SpHz = Spreizung Heizkreis (TVL - TRL)
        # TR_EN: SpHt = spread of heat circuit (TI - TO)
        DtaFieldCalc(category='Berechnet', name='SpHz', color=DtaRGB(128, 0, 0)),
        # TR_DE: SpWq = Spreizung Wärmequelle (TWQein - TWQaus)
        # TR_NE: SpSrc = spread of source circuit (THI - THO)
        DtaFieldCalc(category='Berechnet', name='SpWq', color=DtaRGB(0, 0, 128)),
        # TR_DE: Qth = thermische Leistung (berechnet aus Durchfluss und Spreizung Heizkreis)
        # TR_EN: Qth = thermal power (calculated from flow and spread in heat circuit)
        DtaFieldCalc(category='Berechnet', name='Qth', color=DtaRGB(0, 0, 0)),
    ]

    def __init__(self, data: bytes = b''):
        super().__init__(data)

        self._fields = self.FIELDS_0.copy()

        # read header
        datasets_to_read = 0
        if self._dta.bytes_to_read >= 6:
            self._subversion = self._dta.get_uint32() % 4
            datasets_to_read = self._dta.get_uint16()

        # define dataset length depending on subversion
        if self._subversion == 0:
            self._dataset_length = 54
        elif self._subversion == 1:
            self._dataset_length = 80
            self._fields.extend(self.FIELDS_123)
            self._fields.extend(self.FIELDS_13)
        elif self._subversion == 2:
            self._dataset_length = 72
            self._fields.extend(self.FIELDS_123)
        elif self._subversion == 3:
            self._dataset_length = 98
            self._fields.extend(self.FIELDS_123)
            self._fields.extend(self.FIELDS_13)
            self._fields.extend(self.FIELDS_3)
        else:
            raise DtaViolation('Unsupported DTA v9001 with subversion {}!'.format(self._subversion))  # pragma: no cover
        self._fields.extend(self.FIELDS_CALC)

        # read datasets
        for i in range(datasets_to_read):
            ds: Optional[TDTADataSet] = self.read_dataset()
            if ds is None:
                break
            self._data.append(ds)
        self._calc_values()

        # clean up
        del self._dta

    def _calc_values(self):
        """Calculate additional dataset fields."""
        pos_ai1 = self.field_pos('AI1')
        pos_df = self.field_pos('DF')
        pos_tvl = self.field_pos('TVL')
        pos_trl = self.field_pos('TRL')
        pos_sphz = self.field_pos('SpHz')
        pos_twqein = self.field_pos('TWQein')
        pos_twqaus = self.field_pos('TWQaus')
        pos_spwq = self.field_pos('SpWq')
        pos_qth = self.field_pos('Qth')

        for ds in self._data:
            flow = self.calc_flow_rate_grundfoss_vfs5_5_100(ds[pos_ai1])
            ds[pos_df] = flow
            sp_heat = round((ds[pos_tvl] - ds[pos_trl]) * 10) / 10.0
            ds[pos_sphz] = sp_heat
            ds[pos_spwq] = round((ds[pos_twqein] - ds[pos_twqaus]) * 10) / 10.0
            ds[pos_qth] = self.calc_qth(flow, sp_heat)

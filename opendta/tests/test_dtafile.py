# ----------------------------------------------------------------------
# Copyright (C) 2018  opendta@gmx.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

"""Test DtaFile class and its sub classes for specific DTA file versions."""
import pytest
import os.path
from datetime import datetime
from typing import List
from array import array

from opendta.dtafile import DtaFile
from opendta.dtafile import dta_load_from_bytes, dta_load_from_file


duts: List[dict] = [
    # ---- no data provided ----
    {
        'file': None,
        'version': 0,
        'dataset_count': 0,
        'field_count': 0,
        'fields': [],
    },

    # ---- malformed data ----
    {
        'file': './data/malformed/short_file.dta',
        'version': 0,
        'dataset_count': 0,
        'fields': []
    },

    # ---- V8208 ----
    {
        'file': './data/v8208/v8208.dta',
        'version': 8208,
        'dataset_count': 2880,
        'start': datetime(2013, 10, 9, 2, 20, 4),
        'end': datetime(2013, 10, 11, 3, 32, 15),
    },

    # ---- v8209 ----
    {
        'file': "./data/malformed/v8209_short.dta",
        'version': 8209,
        'dataset_count': 0
    },
    {
        'file': './data/v8209/v8209.dta',
        'version': 8209,
        'dataset_count': 2880,
        'start': datetime(2013, 1, 16, 22, 59, 2),
        'end': datetime(2013, 1, 18, 22, 58, 2),
        'field_count': 56,
        'fields': ['Zeitstempel', 'HUP', 'ZUP', 'BUP', 'ZW2', 'MA1', 'MZ1', 'ZIP', 'VD1', 'VD2', 'VENT', 'AV', 'VBS',
                   'ZW1', 'HD', 'ND', 'MOT', 'ASD', 'EVU', 'TFB1', 'TBW', 'TA', 'TRLext', 'TRL', 'TVL', 'THG', 'TWQaus',
                   'TWQein', 'TRLsoll', 'TMK1soll', 'CfrtPlat', 'AI1DIV', 'SUP', 'FUP2', 'MA2', 'MZ2', 'MA3', 'MZ3',
                   'FUP3', 'ZW3', 'SLP', 'AO1', 'AO2', 'SWT', 'TSS', 'TSK', 'TFB2', 'TFB3', 'TEE', 'AI1', 'TMK2soll',
                   'TMK3soll', 'DF', 'SpHz', 'SpWq', 'Qth'],
        'dataset': array('d',
                         [1358377142.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                          0.0, 0.0, -27.5, 50.7, -2.1, -27.5, 32.1, 32.1, 34.0, 9.8, 9.2, 32.6, 0.0, 3.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 5.0, 3.75, 1.0, -38.5, -38.5, -27.4, -27.4, -27.4, 0.51,
                          30.6, 20.0, 5.5, 0.0, -0.6, 0.0]),
    },

    # ---- v9001 ----
    {
        'file': './data/malformed/v9001_short.dta',
        'version': 9001,
        'dataset_count': 0
    },
    {
        'file': './data/malformed/v9001_short_version.dta',
        'version': 9001,
        'dataset_count': 0
    },
    {
        'file': './data/v9001/v9001_0.dta',
        'version': 9001,
        'subversion': 0,
        'dataset_count': 2880,
        'start': datetime(2013, 3, 28, 19, 36, 20),
        'end': datetime(2013, 3, 30, 19, 35, 20),
        'field_count': 43,
        'fields': ['Zeitstempel', 'TVL', 'TRL', 'TWQein', 'TWQaus', 'THG', 'TBW', 'TFB1', 'TA', 'TRLext', 'TRLsoll',
                   'TMK1soll', 'HD', 'ND', 'MOT', 'ASD', 'EVU', 'HUP', 'ZUP', 'BUP', 'ZW2', 'MA1', 'MZ1', 'ZIP', 'VD1',
                   'VD2', 'VENT', 'AV', 'VBS', 'ZW1', 'TSS', 'TSK', 'TFB2', 'TFB3', 'TEE', 'TMK2soll', 'TMK3soll',
                   'AI1', 'AO1', 'DF', 'SpHz', 'SpWq', 'Qth'],
        'dataset': array('d',
                         [1364499380, 37.7, 30.3, 0.5, -50, 67.2, 75, 75, -10, 5, 33.1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1,
                          0, 0, 1, 0, 0, 0, 1, 1, -0.9, -3.1, 68.6, 0.5, 119, -10, 9.1, 0.086, -0.166, 0, 7.4, 50.5,
                          0]),
    },
    {
        'file': './data/v9001/v9001_1.dta',
        'version': 9001,
        'subversion': 1,
        'dataset_count': 2880,
        'start': datetime(2014, 1, 9, 20, 36, 3),
        'end': datetime(2014, 1, 11, 20, 35, 3),
        'field_count': 49,
        'fields': ['Zeitstempel', 'TVL', 'TRL', 'TWQein', 'TWQaus', 'THG', 'TBW', 'TFB1', 'TA', 'TRLext', 'TRLsoll',
                   'TMK1soll', 'HD', 'ND', 'MOT', 'ASD', 'EVU', 'HUP', 'ZUP', 'BUP', 'ZW2', 'MA1', 'MZ1', 'ZIP', 'VD1',
                   'VD2', 'VENT', 'AV', 'VBS', 'ZW1', 'TSS', 'TSK', 'TFB2', 'TFB3', 'TEE', 'TMK2soll', 'TMK3soll',
                   'AI1', 'AO1', 'AO2', 'Asg.VDi', 'Asg.VDa', 'VDHz', 'UeHz', 'UeHzsoll', 'DF', 'SpHz', 'SpWq', 'Qth'],
    },
    {
        'file': './data/v9001/v9001_2.dta',
        'version': 9001,
        'subversion': 2,
        'dataset_count': 2880,
        'start': datetime(2014, 1, 8, 18, 55, 15),
        'end': datetime(2014, 1, 10, 18, 54, 14),
        'field_count': 47,
        'fields': ['Zeitstempel', 'TVL', 'TRL', 'TWQein', 'TWQaus', 'THG', 'TBW', 'TFB1', 'TA', 'TRLext', 'TRLsoll',
                   'TMK1soll', 'HD', 'ND', 'MOT', 'ASD', 'EVU', 'HUP', 'ZUP', 'BUP', 'ZW2', 'MA1', 'MZ1', 'ZIP', 'VD1',
                   'VD2', 'VENT', 'AV', 'VBS', 'ZW1', 'TSS', 'TSK', 'TFB2', 'TFB3', 'TEE', 'TMK2soll', 'TMK3soll', 'AI1',
                   'AO1', 'AO2', 'Asg.VDi', 'Asg.VDa', 'VDHz', 'DF', 'SpHz', 'SpWq', 'Qth'],
    },
    {
        'file': './data/v9001/v9001_3.dta',
        'version': 9001,
        'subversion': 3,
        'dataset_count': 2880,
        'start': datetime(2015, 11, 23, 15, 58, 50),
        'end': datetime(2015, 11, 25, 18, 49, 1),
        'field_count': 49,
        'fields': ['Zeitstempel', 'TVL', 'TRL', 'TWQein', 'TWQaus', 'THG', 'TBW', 'TFB1', 'TA', 'TRLext', 'TRLsoll',
                   'TMK1soll', 'HD', 'ND', 'MOT', 'ASD', 'EVU', 'HUP', 'ZUP', 'BUP', 'ZW2', 'MA1', 'MZ1', 'ZIP',
                   'VD1', 'VD2', 'VENT', 'AV', 'VBS', 'ZW1', 'TSS', 'TSK', 'TFB2', 'TFB3', 'TEE', 'TMK2soll',
                   'TMK3soll', 'AI1', 'AO1', 'AO2', 'Asg.VDi', 'Asg.VDa', 'VDHz', 'UeHz', 'UeHzsoll', 'DF', 'SpHz',
                   'SpWq', 'Qth'],
    },

    # ---- v9003 ----
    {
        'file': './data/malformed/v9003_short_version.dta',
        'version': 9003,
        'dataset_count': 0
    },
    {
        'file': './data/malformed/v9003_short_fielddef.dta',
        'version': 9003,
        'dataset_count': 0
    },
    {
        'file': './data/malformed/v9003_short_dataset.dta',
        'version': 9003,
        'dataset_count': 0
    },
    {
        'file': './data/v9003/v9003_Grund13_Comfort.dta',
        'version': 9003,
        'dataset_count': 2880,
        'start': datetime(2016, 2, 11, 12, 24, 7),
        'end': datetime(2016, 2, 13, 12, 23, 5),
        'fields': ['Zeitstempel', 'TVL', 'TRL', 'TWE', 'TWA', 'THG', 'TBW', 'TFB1', 'TA', 'TRL_ext', 'TRL_soll',
                   'MK1-Soll', 'HDin', 'NDin', 'MOTin', 'ASDin', 'EVUin', 'HUPout', 'ZUPout', 'BUPout', 'ZW2SSTout',
                   'MA1out', 'MZ1out', 'ZIPout', 'VD1out', 'VD2out', 'VENout', 'AVout', 'VBOout', 'ZW1out', 'SUPout',
                   'FP2out', 'MA2out', 'MZ2out', 'MA3out', 'MZ3out', 'FP3out', 'ZW3SSTout', 'SLPout', 'SWTin', 'TSS',
                   'TSK', 'TFB 2', 'TFB 3', 'TEE', 'MK2-Soll', 'MK3-Soll', 'AnalogIn', 'AnalogOut_1', 'AnalogOut_2'],
    },
    {
        'file': './data/v9003/v9003_Grund13_Comfort_LALIN.dta',
        'version': 9003,
        'dataset_count': 880,
        'start': datetime(2016, 4, 28, 15, 1, 28),
        'end': datetime(2016, 4, 29, 5, 40, 28),
        'fields': ['Zeitstempel', 'TVL', 'TRL', 'TWE', 'TWA', 'THG', 'TBW', 'TFB1', 'TA', 'TRL_ext', 'TRL_soll',
                   'MK1-Soll', 'HDin', 'NDin', 'MOTin', 'ASDin', 'EVUin', 'HUPout', 'ZUPout', 'BUPout', 'ZW2SSTout',
                   'MA1out', 'MZ1out', 'ZIPout', 'VD1out', 'VD2out', 'VENout', 'AVout', 'VBOout', 'ZW1out', 'SUPout',
                   'FP2out', 'MA2out', 'MZ2out', 'MA3out', 'MZ3out', 'FP3out', 'ZW3SSTout', 'SLPout', 'SWTin', 'TSS',
                   'TSK', 'TFB 2', 'TFB 3', 'TEE', 'MK2-Soll', 'MK3-Soll', 'AnalogIn', 'AnalogOut_1', 'AnalogOut_2',
                   'LIN_Verdichter_IN', 'LIN_Verdichter2_IN', 'LIN_Verdichterheizung_OUT', 'LIN_Abtauwunsch',
                   'Ansaug VD', 'Ansaug Verdampfer', 'Temp VDH', 'Temp TWE', 'Druck HD', 'Druck ND',
                   'Verfluessigungstemp.', 'Verdampfungstemp.', 'Ueberhitzung', 'Ueberhitzung Soll',
                   'Schrittmotor Ist'],
    },
    {
        'file': './data/v9003/v9003_Grund13_Compact.dta',
        'version': 9003,
        'dataset_count': 2880,
        'start': datetime(2016, 2, 19, 21, 18, 29),
        'end': datetime(2016, 2, 21, 21, 17, 30),
        'fields': ['Zeitstempel', 'TVL', 'TRL', 'TWE', 'TWA', 'THG', 'TBW', 'TFB1', 'TA', 'TRL_ext', 'TRL_soll',
                   'MK1-Soll', 'HDin', 'NDin', 'MOTin', 'ASDin', 'EVUin', 'HUPout', 'ZUPout', 'BUPout', 'ZW2SSTout',
                   'MA1out', 'MZ1out', 'ZIPout', 'VD1out', 'VD2out', 'VENout', 'AVout', 'VBOout', 'ZW1out', 'SPLin',
                   'IN_SAX', 'OUT_VSK', 'OUT_FRH', 'TZU', 'TAB', 'AIn 2', 'AIn 3', 'AO 3', 'AO 4', 'VZU', 'VAB'],
    },
    {
        'file': './data/v9003/v9003_Grund29.dta',
        'version': 9003,
        'dataset_count': 2880,
        'start': datetime(2015, 7, 10, 16, 46, 6),
        'end': datetime(2015, 7, 12, 16, 45, 6),
        'fields': ['Zeitstempel', 'TVL', 'TRL', 'TRL_ext', 'TRL_soll', 'THG', 'TA', 'TBW', 'TBW_Soll', 'TWE', 'TWA',
                   'TFB1', 'MK1-Soll', 'Multi1', 'Multi2', 'Ansaug VD', 'Ansaug Verdampfer', 'Temp VDH', 'Druck HD',
                   'Druck ND', 'Verfluessigungstemp.', 'Verdampfungstemp.', 'Ueberhitzung', 'Ueberhitzung Soll',
                   'Schrittmotor', 'PWM VBO', 'PWM HUP', 'Durchfluss', 'HDin', 'MOT VD', 'MOTin', 'ASDin', 'EVU 1',
                   'EVU 2', 'IN 7', 'Verdichter', 'AVout', 'VDHZ', 'VBOout', 'ZW1out', 'OUT 7', 'OUT 8', 'OUT 9',
                   'ZW2SSTout', 'ZIPout', 'FP1out', 'MZ1out', 'MA1out', 'ZUPout', 'BUPout', 'HUPout'],
        'dataset': array('d',
                         [1436546766, 44.1, 41.8, 5, 40.8, 83.1, 20.5, 26.8, 50, 4.6, 2.6, 75, 0, 0, 0, 0, 0, 0, 265.8,
                          72.4, 45.4, 0.9, 6.9, 6.9, 11, 99, 99, 207.7, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0,
                          0, 1, 1, 0, 1, 0, 1]),
    },
    {
        'file': './data/v9003/v9003_Grund37.dta',
        'version': 9003,
        'dataset_count': 2880,
        'start': datetime(2015, 10, 30, 23, 53, 49),
        'end': datetime(2015, 11, 1, 23, 52, 49),
        'fields': ['Zeitstempel', 'TVL', 'VL_Soll', 'TRL', 'TRL_ext', 'TRL_soll', 'THG', 'TA', 'TBW', 'TBW_Soll',
                   'BW_oben', 'TWE', 'TWA', 'TFB1', 'MK1-Soll', 'Multi1', 'Multi2', 'Ansaug VD',
                   'Ansaug Verdampfer', 'Temp VDH', 'Druck HD', 'Druck ND', 'Verfluessigungstemp.', 'Verdampfungstemp.',
                   'Ueberhitzung', 'Ueberhitzung Soll', 'Schrittmotor', 'PWM VBO', 'PWM HUP', 'Durchfluss',
                   'Freq_VD', 'Spr. HUP/ZUP', 'Spr. HUP/ZUP Soll', 'Spr. VBO', 'Spr. VBO Soll', 'Mitteltemperatur',
                   'HDin', 'MOT VD', 'MOTin', 'ASDin', 'EVU 1', 'EVU 2', 'IN 7', 'Verdichter', 'AVout', 'VDHZ',
                   'VBOout', 'ZW1out', 'OUT 7', 'OUT 8', 'OUT 9', 'ZW2SSTout', 'ZIPout', 'FP1out', 'MZ1out', 'MA1out',
                   'ZUPout', 'BUPout', 'HUPout'],
    },
    {
        'file': './data/v9003/v9003_Grund37_Comfort2.dta',
        'version': 9003,
        'dataset_count': 2880,
        'start': datetime(2016, 10, 13, 10, 27, 23),
        'end': datetime(2016, 10, 15, 10, 26, 23),
        'fields': ['Zeitstempel', 'TVL', 'VL_Soll', 'TRL', 'TRL_ext', 'TRL_soll', 'THG', 'TA', 'TBW', 'TBW_Soll',
                   'BW_oben', 'TWE', 'TWA', 'TFB1', 'MK1-Soll', 'Multi1', 'Multi2', 'Ansaug VD',
                   'Ansaug Verdampfer', 'Temp VDH', 'Druck HD', 'Druck ND', 'Verfluessigungstemp.', 'Verdampfungstemp.',
                   'Ueberhitzung', 'Ueberhitzung Soll', 'Schrittmotor', 'PWM VBO', 'PWM HUP', 'Durchfluss',
                   'Freq_VD', 'Spr. HUP/ZUP', 'Spr. HUP/ZUP Soll', 'Spr. VBO', 'Spr. VBO Soll', 'Mitteltemperatur',
                   'HDin', 'MOT VD', 'MOTin', 'ASDin', 'EVU 1', 'EVU 2', 'IN 7', 'Verdichter', 'AVout', 'VDHZ',
                   'VBOout', 'ZW1out', 'OUT 7', 'OUT 8', 'OUT 9', 'ZW2SSTout', 'ZIPout', 'FP1out', 'MZ1out', 'MA1out',
                   'ZUPout', 'BUPout', 'HUPout', 'SUPout', 'FP2out', 'MA2out', 'MZ2out', 'MA3out', 'MZ3out', 'FP3out',
                   'ZW3SSTout', 'SLPout', 'SWTin', 'TSS', 'TSK', 'TFB 2', 'TFB 3', 'TEE', 'MK2-Soll', 'MK3-Soll',
                   'AnalogIn 21', 'AnalogIn 22', 'AnalogOut 21', 'AnalogOut 22'],
    },
    {
        'file': './data/v9003/v9003_Grund37_RBE.dta',
        'version': 9003,
        'dataset_count': 2880,
        'start': datetime(2016, 8, 10, 12, 11, 35),
        'end': datetime(2016, 8, 12, 12, 10, 35),
        'fields': ['Zeitstempel', 'TVL', 'VL_Soll', 'TRL', 'TRL_ext', 'TRL_soll', 'THG', 'TA', 'TBW', 'TBW_Soll',
                   'BW_oben', 'TWE', 'TWA', 'TFB1', 'MK1-Soll', 'Multi1', 'Multi2', 'Ansaug VD',
                   'Ansaug Verdampfer', 'Temp VDH', 'Druck HD', 'Druck ND', 'Verfluessigungstemp.', 'Verdampfungstemp.',
                   'Ueberhitzung', 'Ueberhitzung Soll', 'Schrittmotor', 'PWM VBO', 'PWM HUP', 'Durchfluss',
                   'Freq_VD', 'Spr. HUP/ZUP', 'Spr. HUP/ZUP Soll', 'Spr. VBO', 'Spr. VBO Soll', 'Mitteltemperatur',
                   'HDin', 'MOT VD', 'MOTin', 'ASDin', 'EVU 1', 'EVU 2', 'IN 7', 'Verdichter', 'AVout', 'VDHZ',
                   'VBOout', 'ZW1out', 'OUT 7', 'OUT 8', 'OUT 9', 'ZW2SSTout', 'ZIPout', 'FP1out', 'MZ1out', 'MA1out',
                   'ZUPout', 'BUPout', 'HUPout', 'Start Kuhlung', 'RT Ist', 'RT Soll', 'RBE Einfluss',
                   'RBE WW Soll extra'],
    },
    {
        'file': './data/v9003/v9003_Grund44_Comfort_LALIN.dta',
        'version': 9003,
        'dataset_count': 2880,
        'start': datetime(2016, 7, 13, 15, 33, 17),
        'end': datetime(2016, 7, 15, 15, 32, 17),
        'fields': ['Zeitstempel', 'TVL', 'TRL', 'TWE', 'TWA', 'THG', 'TBW', 'TFB1', 'TA', 'TRL_ext', 'TRL_soll',
                   'MK1-Soll', 'WMZ Hz', 'WMZ Bw', 'WMZ Sw', 'Hz.Min', 'Hz.Max', 'MK1.Min', 'MK1.Max', 'MK2.Min',
                   'MK2.Max', 'MK3.Min', 'MK3.Max', 'HZ floor count', 'HZ floor open', 'HZ drive count', 'HZ RT ist',
                   'HZ RT soll', 'MK1 floor count', 'MK1 floor open', 'MK1 drive count', 'MK1 RT ist', 'MK1 RT soll',
                   'MK2 floor count', 'MK2 floor open', 'MK2 drive count', 'MK2 RT ist', 'MK2 RT soll',
                   'MK3 floor count', 'MK3 floor open', 'MK3 drive count', 'MK3 RT ist', 'MK3 RT soll', 'HDin', 'NDin',
                   'MOTin', 'ASDin', 'EVUin', 'HUPout', 'ZUPout', 'BUPout', 'ZW2SSTout', 'MA1out', 'MZ1out', 'ZIPout',
                   'VD1out', 'VD2out', 'VENout', 'AVout', 'VBOout', 'ZW1out', 'SUPout', 'FP2out', 'MA2out', 'MZ2out',
                   'MA3out', 'MZ3out', 'FP3out', 'ZW3SSTout', 'SLPout', 'SWTin', 'TSS', 'TSK', 'TFB 2', 'TFB 3', 'TEE',
                   'MK2-Soll', 'MK3-Soll', 'AnalogIn', 'AnalogOut_1', 'AnalogOut_2', 'LIN_Verdichter_IN',
                   'LIN_Verdichter2_IN', 'LIN_Verdichterheizung_OUT', 'LIN_Abtauwunsch', 'Ansaug VD',
                   'Ansaug Verdampfer', 'Temp VDH', 'Temp TWE', 'Druck HD', 'Druck ND', 'Verfluessigungstemp.',
                   'Verdampfungstemp.', 'Ueberhitzung', 'Ueberhitzung Soll', 'Schrittmotor Ist'],
    },
    {
        'file': './data/v9003/v9003_Grund44_Smart_Comfort2.dta',
        'version': 9003,
        'dataset_count': 2880,
        'start': datetime(2019, 6, 2, 11, 15, 39),
        'end': datetime(2019, 6, 4, 11, 14, 39),
        'fields': ['Zeitstempel', 'TVL', 'VL_Soll', 'TRL', 'TRL_ext', 'TRL_soll', 'THG', 'TA', 'TBW', 'TBW_Soll',
                   'BW_oben', 'TWE', 'TWA', 'TFB1', 'MK1-Soll', 'Multi1', 'Multi2', 'Ansaug VD',
                   'Ansaug Verdampfer', 'Temp VDH', 'Druck HD', 'Druck ND', 'Verfluessigungstemp.', 'Verdampfungstemp.',
                   'Ueberhitzung', 'Ueberhitzung Soll', 'Schrittmotor', 'PWM VBO', 'PWM HUP', 'Durchfluss',
                   'Freq_VD', 'Spr. HUP/ZUP', 'Spr. HUP/ZUP Soll', 'Spr. VBO', 'Spr. VBO Soll', 'Mitteltemperatur',
                   'AO 1', 'AO 2', 'AO 3', 'AO 4', 'Error ID', 'HDin', 'MOT VD', 'MOTin', 'ASDin', 'EVU 1', 'EVU 2',
                   'IN 7', 'Verdichter', 'AVout', 'VDHZ', 'VBOout', 'ZW1out', 'OUT 7', 'OUT 8', 'OUT 9', 'ZW2SSTout',
                   'ZIPout', 'FP1out', 'MZ1out', 'MA1out', 'ZUPout', 'BUPout', 'HUPout', 'Bivalence', 'Demand',
                   'Hz.Min', 'Hz.Max', 'MK1.Min', 'MK1.Max', 'MK2.Min', 'MK2.Max', 'MK3.Min', 'MK3.Max',
                   'Hz floor count', 'Hz floor open', 'Hz drive count', 'Hz RT ist', 'Hz RT soll', 'MK1 floor count',
                   'MK1 floor open', 'MK1 drive count', 'MK1 RT ist', 'MK1 RT soll', 'MK2 floor count',
                   'MK2 floor open', 'MK2 drive count', 'MK2 RT ist', 'MK2 RT soll', 'MK3 floor count',
                   'MK3 floor open', 'MK3 drive count', 'MK3 RT ist', 'MK3 RT soll', 'Room 1 HeatConf',
                   'Room 1 Heat ID', 'Room 1 CoolConf', 'Room 1 Cool ID', 'Room 1 TarTemp', 'Room 1 CurTemp',
                   'Room 1 ReqTemp', 'Room 2 HeatConf', 'Room 2 Heat ID', 'Room 2 CoolConf', 'Room 2 Cool ID',
                   'Room 2 TarTemp', 'Room 2 CurTemp', 'Room 2 ReqTemp', 'Room 3 HeatConf', 'Room 3 Heat ID',
                   'Room 3 CoolConf', 'Room 3 Cool ID', 'Room 3 TarTemp', 'Room 3 CurTemp', 'Room 3 ReqTemp',
                   'Room 4 HeatConf', 'Room 4 Heat ID', 'Room 4 CoolConf', 'Room 4 Cool ID', 'Room 4 TarTemp',
                   'Room 4 CurTemp', 'Room 4 ReqTemp', 'Room 5 HeatConf', 'Room 5 Heat ID', 'Room 5 CoolConf',
                   'Room 5 Cool ID', 'Room 5 TarTemp', 'Room 5 CurTemp', 'Room 5 ReqTemp', 'Room 6 HeatConf',
                   'Room 6 Heat ID', 'Room 6 CoolConf', 'Room 6 Cool ID', 'Room 6 TarTemp', 'Room 6 CurTemp',
                   'Room 6 ReqTemp', 'Room 7 HeatConf', 'Room 7 Heat ID', 'Room 7 CoolConf', 'Room 7 Cool ID',
                   'Room 7 TarTemp', 'Room 7 CurTemp', 'Room 7 ReqTemp', 'Room 8 HeatConf', 'Room 8 Heat ID',
                   'Room 8 CoolConf', 'Room 8 Cool ID', 'Room 8 TarTemp', 'Room 8 CurTemp', 'Room 8 ReqTemp',
                   'Room 9 HeatConf', 'Room 9 Heat ID', 'Room 9 CoolConf', 'Room 9 Cool ID', 'Room 9 TarTemp',
                   'Room 9 CurTemp', 'Room 9 ReqTemp', 'Room 10 HeatConf', 'Room 10 Heat ID', 'Room 10 CoolConf',
                   'Room 10 Cool ID', 'Room 10 TarTemp', 'Room 10 CurTemp', 'Room 10 ReqTemp', 'Room 11 HeatConf',
                   'Room 11 Heat ID', 'Room 11 CoolConf', 'Room 11 Cool ID', 'Room 11 TarTemp', 'Room 11 CurTemp',
                   'Room 11 ReqTemp', 'Room 12 HeatConf', 'Room 12 Heat ID', 'Room 12 CoolConf', 'Room 12 Cool ID',
                   'Room 12 TarTemp', 'Room 12 CurTemp', 'Room 12 ReqTemp', 'Room 13 HeatConf', 'Room 13 Heat ID',
                   'Room 13 CoolConf', 'Room 13 Cool ID', 'Room 13 TarTemp', 'Room 13 CurTemp', 'Room 13 ReqTemp',
                   'Room 14 HeatConf', 'Room 14 Heat ID', 'Room 14 CoolConf', 'Room 14 Cool ID', 'Room 14 TarTemp',
                   'Room 14 CurTemp', 'Room 14 ReqTemp', 'Room 15 HeatConf', 'Room 15 Heat ID', 'Room 15 CoolConf',
                   'Room 15 Cool ID', 'Room 15 TarTemp', 'Room 15 CurTemp', 'Room 15 ReqTemp', 'Room 16 HeatConf',
                   'Room 16 Heat ID', 'Room 16 CoolConf', 'Room 16 Cool ID', 'Room 16 TarTemp', 'Room 16 CurTemp',
                   'Room 16 ReqTemp', 'Room 17 HeatConf', 'Room 17 Heat ID', 'Room 17 CoolConf', 'Room 17 Cool ID',
                   'Room 17 TarTemp', 'Room 17 CurTemp', 'Room 17 ReqTemp', 'Room 18 HeatConf', 'Room 18 Heat ID',
                   'Room 18 CoolConf', 'Room 18 Cool ID', 'Room 18 TarTemp', 'Room 18 CurTemp', 'Room 18 ReqTemp',
                   'Room 19 HeatConf', 'Room 19 Heat ID', 'Room 19 CoolConf', 'Room 19 Cool ID', 'Room 19 TarTemp',
                   'Room 19 CurTemp', 'Room 19 ReqTemp', 'Room 20 HeatConf', 'Room 20 Heat ID', 'Room 20 CoolConf',
                   'Room 20 Cool ID', 'Room 20 TarTemp', 'Room 20 CurTemp', 'Room 20 ReqTemp', 'Room 21 HeatConf',
                   'Room 21 Heat ID', 'Room 21 CoolConf', 'Room 21 Cool ID', 'Room 21 TarTemp', 'Room 21 CurTemp',
                   'Room 21 ReqTemp', 'Room 22 HeatConf', 'Room 22 Heat ID', 'Room 22 CoolConf', 'Room 22 Cool ID',
                   'Room 22 TarTemp', 'Room 22 CurTemp', 'Room 22 ReqTemp', 'Room 23 HeatConf', 'Room 23 Heat ID',
                   'Room 23 CoolConf', 'Room 23 Cool ID', 'Room 23 TarTemp', 'Room 23 CurTemp', 'Room 23 ReqTemp',
                   'Room 24 HeatConf', 'Room 24 Heat ID', 'Room 24 CoolConf', 'Room 24 Cool ID', 'Room 24 TarTemp',
                   'Room 24 CurTemp', 'Room 24 ReqTemp', 'SUPout', 'FP2out', 'MA2out', 'MZ2out', 'MA3out', 'MZ3out',
                   'FP3out', 'ZW3SSTout', 'SLPout', 'SWTin', 'TSS', 'TSK', 'TFB 2', 'TFB 3', 'TEE', 'MK2-Soll',
                   'MK3-Soll', 'AnalogIn 21', 'AnalogIn 22', 'AO 21', 'AO 22'],
    },
    {
        'file': './data/v9003/v9003_Grund66_GLT.dta',
        'version': 9003,
        'dataset_count': 2880,
        'start': datetime(2018, 2, 27, 16, 40, 1),
        'end': datetime(2018, 3, 1, 16, 39, 1),
        'fields': ['Zeitstempel', 'TVL', 'VL_Soll', 'TRL', 'TRL_ext', 'TRL_soll', 'THG', 'TA', 'TBW', 'TBW_Soll',
                   'BW_oben', 'TWE', 'TWA', 'TFB1', 'MK1-Soll', 'Multi1', 'Multi2', 'Ansaug VD',
                   'Ansaug Verdampfer', 'Temp VDH', 'Druck HD', 'Druck ND', 'Verfluessigungstemp.', 'Verdampfungstemp.',
                   'Ueberhitzung', 'Ueberhitzung Soll', 'Schrittmotor', 'PWM VBO', 'PWM HUP', 'Durchfluss',
                   'Freq_VD', 'Spr. HUP/ZUP', 'Spr. HUP/ZUP Soll', 'Spr. VBO', 'Spr. VBO Soll', 'Mitteltemperatur',
                   'Hz.Min', 'Hz.Max', 'MK1.Min', 'MK1.Max', 'MK2.Min', 'MK2.Max', 'MK3.Min', 'MK3.Max',
                   'HZ floor count', 'HZ floor open', 'HZ drive count', 'HZ RT ist', 'HZ RT soll', 'MK1 floor count',
                   'MK1 floor open', 'MK1 drive count', 'MK1 RT ist', 'MK1 RT soll', 'MK2 floor count',
                   'MK2 floor open', 'MK2 drive count', 'MK2 RT ist', 'MK2 RT soll', 'MK3 floor count',
                   'MK3 floor open', 'MK3 drive count', 'MK3 RT ist', 'MK3 RT soll', 'HDin', 'MOT VD', 'MOTin', 'ASDin',
                   'EVU 1', 'EVU 2', 'IN 7', 'Verdichter', 'AVout', 'VDHZ', 'VBOout', 'ZW1out', 'OUT 7', 'OUT 8',
                   'OUT 9', 'ZW2SSTout', 'ZIPout', 'FP1out', 'MZ1out', 'MA1out', 'ZUPout', 'BUPout', 'HUPout', 'AT',
                   'RL_Soll', 'MK1 Soll', 'MK2 Soll', 'MK3 Soll', 'Einst BWS akt', 'AnzahlVD',
                   'Betriebsart Heizen', 'Betriebsart Brauchwasser', 'Betriebsart MK2', 'Betriebsart MK3',
                   'Betriebsart Kuehlen', 'Betriebsart Lueftung', 'Betriebsart Schwimmbad', 'Fehlerreset Datenlog',
                   'HUPout', 'VENout', 'ZUPout', 'BUPout', 'VBOout', 'ZIPout', 'FP2out', 'FP3out', 'SLPout', 'SUPout',
                   'OUT VSK', 'OUT FRH'],
    },
    {
        'file': './data/v9003/v9003_Heizkreis_KaeKreis_HeatSource_Anlagenstatus.dta',
        'version': 9003,
        'dataset_count': 2880,
        'start': datetime(2019, 11, 4, 21, 37, 19),
        'end': datetime(2019, 11, 6, 21, 36, 19),
        'fields': ['Zeitstempel', 'Vorlauf', 'Rucklauf', 'Ruecklext', 'RL_Soll', 'Aussent',
                   'Mitteltemperatur', 'BW_oben', 'BW_Ist', 'BW_Soll', 'MK1_Vorlauf',
                   'MK1VL_Soll', 'Multi1', 'Multi2', 'PWM_UWP', 'ABS_DFS', 'Spr. HUP/ZUP Soll',
                   'Spr. VBO Soll', 'AO 1', 'AO 2', 'Vorlaufmax', 'EVU', 'EVU 2', 'MOT',
                   'ASD', 'STB_EStab', 'HUP', 'ZUP', 'BUP', 'FUP1', 'Mischer1Auf',
                   'Mischer1Zu', 'ZIP', 'ZWE1', 'ZWE2_SST', 'OUT 7', 'OUT 8', 'OUT 9',
                   'Therm_Desinfekt', 'Heissgas', 'THGmax', 'Verdampfungstemp',
                   'Verfluessigungstemp', 'Ueberhitzung', 'Ueberhitzung_Soll', 'EEV_opening',
                   'Ansaug_Verdichter', 'Ansaug_Verdampfer', 'LA_VD_Soll', 'Signal_VD_aktiv',
                   'VD_Heizung', 'ND', 'HD', 'HD', 'IN 3', 'Verdichter', 'VD_Heizung',
                   'Abtauventil', 'WQ_Ein', 'Kaltekreis', 'PWM Text_Ventil_BOSUP', 'Ventil_BOSUP',
                   'Fehlerspeicher', 'Biv_Sufe', 'Signal_HZRegler', 'FreigZWE', 'Timer_BIV_mehr',
                   'Zugang', 'SW_Stand', 'WP_Typ', 'SSP_Zeit', 'Signal_BivRed',
                   'coolingClearance_Rev.', 'Kuehl_umschaltung', 'Netzein_ready', 'EVU_ready',
                   'Signal_CoolingClearance', 'Zwangsheizung', 'Zwangsbrauchwasser'],
    },
    {
        'file': './data/v9003/v9003_Heizkreis_KaeKreis_HeatSource_Anlagenstatus_2.dta',
        'version': 9003,
        'dataset_count': 492,
        'start': datetime(2019, 12, 27, 13, 23, 7),
        'end': datetime(2019, 12, 27, 21, 34, 7),
        'fields': ['Zeitstempel', 'Vorlauf', 'VL_Soll', 'Rucklauf', 'Ruecklext', 'RL_Soll',
                   'Aussent', 'BW_Ist', 'BW_Soll', 'MK1_Vorlauf', 'MK1VL_Soll', 'PWM_UWP',
                   'PWM_UWP 2', 'ABS_DFS', 'LA_DFS_Soll', 'AO 1', 'AO 2', 'LA_VDStop', 'Vorlaufmax',
                   'EVU', 'EVU 2', 'MOT', 'ASD', 'HUP', 'ZUP', 'BUP', 'FUP1',
                   'Mischer1Auf', 'Mischer1Zu', 'ZIP', 'KS', 'BLP', 'ZWE1', 'ZWE2_SST',
                   'Therm_Desinfekt', 'Heissgas', 'Verdampfungstemp', 'Verfluessigungstemp',
                   'Ueberhitzung', 'Ueberhitzung_Soll', 'EEV_opening', 'Ansaug_Verdichter',
                   'Ansaug_Verdampfer', 'Signal_VD_aktiv', 'VD_Heizung', 'VD-Heizung Soll', 'ND',
                   'HD', 'Verdichter', 'Verdichter 2', 'VD_Heizung', 'Signal_ABT',
                   'Abtauwunsch', 'Abtauventil', 'VD_RPM Text_Aktuell', 'VD_RPM Text_Sollwert',
                   'VD_RPM Text_Min', 'VD_RPM Text_Max', 'LA_PCB_Temp', 'Drivestate', 'Motor1CurrentOut',
                   'Motor1PowerOut', 'PFC Temp', 'PM Temp', 'WQ_Ein', 'LA_FanVoltage', 'Ventil_BOSUP',
                   'Fehlerspeicher', 'Kapazitat', 'Defrost_Demand', 'Biv_Sufe', 'Anteil P',
                   'Anteil I', 'Anteil D', 'Timer_VD1_Laufz', 'Signal_HZRegler', 'FreigZWE', 'Timer_BIV_mehr',
                   'Zugang', 'SW_Stand', 'WP_Typ', 'Silent Mode', 'SSP_Zeit', 'Signal_BivRed',
                   'coolingClearance_Rev.', 'Kuehl_umschaltung', 'Netzein_ready', 'EVU_ready',
                   'Signal_CoolingClearance', 'Zwangsheizung', 'Zwangsbrauchwasser'],
    },
    {
        'file': './data/v9003/v9003_HZIO_HMI_Inverter_ASB_Fan.dta',
        'version': 9003,
        'dataset_count': 2880,
        'start': datetime(2019, 11, 2, 14, 11, 4),
        'end': datetime(2019, 11, 4, 16, 16, 20),
        'fields': ['Zeitstempel', 'TVL', 'TRL', 'TBW', 'TBW_Soll', 'TFB1', 'TA', 'TRL_ext', 'TRL_soll', 'MK1-Soll',
                   'PWM 1', 'PWM 2', 'DFS', 'DFS Soll', 'VD Stop', 'AO 1', 'AO 2', 'MOTin', 'ASDin', 'EVU 1', 'EVU 2',
                   'HUPout', 'ZUPout', 'BUPout', 'MA1out', 'MZ1out', 'FP1out', 'ZIPout', 'Signal_VD_aktiv', 'AVout',
                   'Signal_ABT', 'VBOout', 'ZW1out', 'Bivalence', 'Demand', 'Waermeleistung', 'AbtZaehler',
                   'AbtZaehlerZiel', 'Abtaubedarf [%]', 'Error ID', 'Anteil P', 'Anteil I', 'Anteil D',
                   'Timer_VD1_Laufz', 'VD1out', 'Silent Mode', 'Speed', 'Speed Setpoint', 'Stepped Speed',
                   'Min. Speed', 'Max. Speed', 'Envelope Nr', 'Drivestate', 'Fault 1', 'Fault 2', 'CurrentOutLimit',
                   'PowerOutLimit', 'CurrentInLimit', 'pfcCurrentInput', 'PCB Temp', 'Motor1CurrentOut',
                   'Motor1PowerOut', 'PFC Temp', 'Highpressure ', 'Lowpressure  ', 'Verdampfungstemp.',
                   'Kondensationstemp.', 'TWE', 'TSG1', 'THG', 'THG Grenze', 'Superheat-Setpoint', 'Superheat-Is',
                   'Superheat-Error RMS', 'EEV % Heating', 'EEV % Cooling', 'EEVH TFL1', 'EEVC TFL2',
                   'UnterkÃ¼hlung EEV', 'Fan speed-Is', 'Fan speed-Setpoint '],
    },
]
ids = ['no data' if x['file'] is None else os.path.basename(x['file']) for x in duts]


class TestDtaFile:
    """Define DtaFile test class."""

    @pytest.fixture(scope='class', params=duts, ids=ids)
    def dut(self, request) -> dict:
        """Extract request parameters."""
        return request.param

    @pytest.fixture(scope='class')
    def dta(self, dut) -> DtaFile:
        """Create DtaFile instance from DUT parameters."""
        if dut['file'] is not None:
            dir = os.path.dirname(os.path.realpath(__file__))
            return dta_load_from_file(os.path.join(dir, dut['file']))
        else:
            return dta_load_from_bytes(b'')

    def test_version(self, dta: DtaFile, dut: dict):
        """Test DTA file version."""
        assert dta.version == dut['version']

    def test_subversion(self, dta: DtaFile, dut: dict):
        """Test DTA file sub-version."""
        if 'subversion' not in dut:
            assert True
        else:
            assert dta.subversion == dut['subversion']

    def test_field_count(self, dta: DtaFile, dut: dict):
        """Test field count."""
        if 'field_count' not in dut:
            assert True
        else:
            assert dta.field_count == dut['field_count']

    def test_fields(self, dta: DtaFile, dut: dict):
        """Test field names."""
        if 'fields' not in dut:
            assert True
        else:
            assert dta.field_names == dut['fields']

    def test_dataset_count(self, dta: DtaFile, dut: dict):
        """Test dataset count."""
        assert len(dta) == dut['dataset_count']

    def test_dataset(self, dta: DtaFile, dut: dict):
        """Test first dataset content."""
        if 'dataset' not in dut:
            assert True
        else:
            assert next(dta) == dut['dataset']

    def test_start(self, dta: DtaFile, dut: dict):
        """Test start date/time."""
        if 'start' not in dut:
            assert True
        else:
            assert datetime.utcfromtimestamp(dta[0][0]) == dut['start']

    def test_end(self, dta: DtaFile, dut: dict):
        """Test end date/time."""
        if 'end' not in dut:
            assert True
        else:
            assert datetime.utcfromtimestamp(dta[-1][0]) == dut['end']

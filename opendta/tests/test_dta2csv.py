# ----------------------------------------------------------------------
# Copyright (C) 2018  opendta@gmx.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

"""Test dta2csv class."""
import pytest
import os
from opendta.dta2csv import Dta2Csv
from gettext import gettext as _
import locale

# load default locale from system and define CSV separator
try:
    locale.setlocale(locale.LC_ALL, locale='')
except locale.Error:  # pragma: no cover
    pass
SEP: str = ';' if locale.localeconv()['decimal_point'] == ',' else ','

DIR: str = os.path.dirname(os.path.realpath(__file__))

# ---- argument/option tests ----
arg_tests = [
    {
        'id': 'no_args',
        'args': [],
        'code': 2,
        'stderr': 'usage: '
    },
    {
        'id': 'wrong_args',
        'args': ['--invalid_arg', 'dta_file'],
        'code': 2,
        'stderr': 'usage: '
    },
]
arg_test_ids = [x['id'] for x in arg_tests]

# ---- file test ----
file_tests = [
    {
        'id': 'missing_file',
        'args': ['missing_dta_file'],
        'code': 1,
        'log': [_('File "{file_name}" not found!').format(file_name='missing_dta_file')],
        'stdout': None
    },
    {
        'id': 'unsupported_file',
        'args': ['-v', os.path.join(DIR, 'data/malformed/no_dta.dta')],
        'code': 2,
        'log': [_('DTA file: ')],
        'stdout': None
    },
    {
        'id': 'v8209.dta',
        'args': [os.path.join(DIR, 'data/v8209/v8209.dta')],
        'code': 0,
        'log': [],
        'stdout':
            _('version') + SEP + '8209' + SEP + '0x2011\n' +
            _('subversion') + SEP + '0' + SEP + '0x0\n' +
            _('field count') + SEP + '56\n' +
            _('dataset count') + SEP + '2880\n\n' +
            SEP.join(['#', '', _('Zeitstempel'), _('HUP'), _('ZUP'), _('BUP'), _('ZW2'), _('MA1'), _('MZ1'),
                      _('ZIP'), _('VD1')])
    },
]
file_test_ids = [x['id'] for x in file_tests]


class TestDta2Csv:

    @pytest.fixture(scope='class', params=arg_tests, ids=arg_test_ids)
    def dut_args(self, request) -> dict:
        return request.param

    def test_args(self, capsys, dut_args: dict):
        with pytest.raises(SystemExit) as e:
            Dta2Csv.run(dut_args['args'])
        capture: str = capsys.readouterr().err
        assert e.type == SystemExit
        assert e.value.code == dut_args['code']
        assert capture.startswith(dut_args['stderr'])

    @pytest.fixture(scope='class', params=file_tests, ids=file_test_ids)
    def dut_file(self, request) -> dict:
        return request.param

    def test_file(self, caplog, capsys, dut_file: dict):
        assert Dta2Csv.run(dut_file['args']) == dut_file['code']
        for log in dut_file['log']:
            assert caplog.text.find(log) != -1
        if dut_file['stdout'] is not None:
            capture = capsys.readouterr().out
            assert capture.startswith(dut_file['stdout'])

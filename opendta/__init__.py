# ----------------------------------------------------------------------
# Copyright (C) 2018  opendta@gmx.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

"""Define package information for setuptool and internal usage."""
from .version import VERSION
__version__ = VERSION

pkg_info = {
    'name': 'opendta',
    'version': __version__,
    'description': 'Decode and visualize DTA files from ait/Novelan heat pumps.',
    'keywords': 'DTA heatpump "alpha innotec" ait Novelan',
    'url': 'https://gitlab.com/opendta/opendta_py',
    'author': 'opendta',
    'author_email': 'opendta@gmx.de',
    'license': 'GPLv3+',
    'classifiers': [
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Home Automation',
        'Topic :: Scientific/Engineering :: Information Analysis'
    ],
}

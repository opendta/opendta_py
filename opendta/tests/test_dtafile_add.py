# ------------------------------------------------------------------------------
#  Copyright (C) 2020  opendta@gmx.de
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

"""Test DTA dataset merge."""

import pytest
import os
from datetime import datetime

from opendta.dtafile import dta_load_from_file, DtaViolation, DtaFile


@pytest.fixture(scope='class')
def class_data(request):
    data_dir = os.path.dirname(os.path.realpath(__file__))
    request.cls.data_dir = data_dir

    # load DTA files
    request.cls.dta8209_0 = dta_load_from_file(os.path.join(data_dir, './data/v8209/v8209_add0.dta'))
    request.cls.dta8209_1 = dta_load_from_file(os.path.join(data_dir, './data/v8209/v8209_add1.dta'))
    request.cls.dta8209_2 = dta_load_from_file(os.path.join(data_dir, './data/v8209/v8209_add2.dta'))
    request.cls.dta8209_3 = dta_load_from_file(os.path.join(data_dir, './data/v8209/v8209_add3.dta'))
    request.cls.dta9001_0 = dta_load_from_file(os.path.join(data_dir, './data/v9001/v9001_0.dta'))
    request.cls.dta9001_1 = dta_load_from_file(os.path.join(data_dir, './data/v9001/v9001_1.dta'))

    yield


@pytest.mark.usefixtures('class_data')
class TestDtaFileAdd:
    """Define DtaFile merge test class."""

    @staticmethod
    def _copy_dta(dta: DtaFile) -> DtaFile:
        newdta: DtaFile = DtaFile()
        newdta += dta
        return newdta

    def test_compatible(self):
        assert self.dta8209_1.compatible(self.dta8209_2)  # same version, same fields
        assert not self.dta8209_1.compatible(self.dta9001_0)  # different version
        assert not self.dta9001_0.compatible(self.dta9001_1)  # different fields
        dta: DtaFile = DtaFile()
        assert self.dta9001_0.compatible(dta)  # dta is empty
        assert dta.compatible(self.dta9001_1)  # dta is empty

    def test_diff_version(self):
        with pytest.raises(DtaViolation, match=r".*have different versions!"):
            self.dta8209_1 += self.dta9001_0

    def test_diff_fields(self):
        with pytest.raises(DtaViolation, match=r".*have different fields!"):
            self.dta9001_0 += self.dta9001_1

    def test_add_empty(self):
        dta = self._copy_dta(self.dta8209_0)
        dta += DtaFile()
        assert len(dta) == 2880
        assert datetime.utcfromtimestamp(dta[0][0]) == datetime(2020, 1, 27, 0, 42, 35)
        assert datetime.utcfromtimestamp(dta[-1][0]) == datetime(2020, 1, 29, 0, 41, 35)

    def test_add_to_empty(self):
        dta: DtaFile = DtaFile()
        dta += self.dta8209_0
        assert len(dta) == 2880
        assert datetime.utcfromtimestamp(dta[0][0]) == datetime(2020, 1, 27, 0, 42, 35)
        assert datetime.utcfromtimestamp(dta[-1][0]) == datetime(2020, 1, 29, 0, 41, 35)

    def test_add_before(self):
        dta = self._copy_dta(self.dta8209_1)
        dta += self.dta8209_0
        assert len(dta) == 5760
        assert datetime.utcfromtimestamp(dta[0][0]) == datetime(2020, 1, 27, 0, 42, 35)
        assert datetime.utcfromtimestamp(dta[-1][0]) == datetime(2020, 1, 31, 0, 41, 35)

    def test_add_overlap_before(self):
        dta = self._copy_dta(self.dta8209_2)
        dta += self.dta8209_1
        assert len(dta) == 4320
        assert datetime.utcfromtimestamp(dta[0][0]) == datetime(2020, 1, 29, 0, 42, 35)
        assert datetime.utcfromtimestamp(dta[-1][0]) == datetime(2020, 2, 1, 0, 41, 35)

    def test_add_overlap_after(self):
        dta = self._copy_dta(self.dta8209_1)
        dta += self.dta8209_2
        assert len(dta) == 4320
        assert datetime.utcfromtimestamp(dta[0][0]) == datetime(2020, 1, 29, 0, 42, 35)
        assert datetime.utcfromtimestamp(dta[-1][0]) == datetime(2020, 2, 1, 0, 41, 35)

    def test_add_after(self):
        dta = self._copy_dta(self.dta8209_2)
        dta += self.dta8209_3
        assert len(dta) == 5760
        assert datetime.utcfromtimestamp(dta[0][0]) == datetime(2020, 1, 30, 0, 42, 35)
        assert datetime.utcfromtimestamp(dta[-1][0]) == datetime(2020, 2, 4, 0, 41, 35)

# ----------------------------------------------------------------------
# Copyright (C) 2018  opendta@gmx.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

"""Define base class for DTA files."""
from array import array
from typing import List, Generator, Optional
import struct

from .binarydata import BinaryData
from .dtafield import DtaField

# data type for DTA dataset
TDTADataSet = array


# DTA Exception
class DtaViolation(Exception):
    """Exception to be raised for DTA errors."""
    pass


class DtaFile(object):
    """Define base class to process DTA files."""

    def __init__(self, data: bytes = b''):
        """Define constructor."""
        self._dta: BinaryData = BinaryData(data)
        self._dta.pos = 4  # 4 bytes version already read
        self._dataset_length = 99999
        self._fields: List[DtaField] = []
        self._field_names: Optional[List[str]] = None
        self._data: List[TDTADataSet] = []
        self._version: int = self.get_version(data)
        self._subversion: int = 0

        # for iterator
        self._iter_pos: int = 0

    @property
    def version(self) -> int:
        """Return DTA version."""
        return self._version

    @property
    def subversion(self) -> int:
        """Return sub version."""
        return self._subversion

    @property
    def fields(self) -> Generator[DtaField, None, None]:
        """Return generator for field names."""
        for field in self._fields:
            yield field

    @property
    def field_names(self) -> List[str]:
        if self._field_names is not None:
            return self._field_names
        res: List[str] = []
        for field in [f for f in self._fields if len(f) > 0]:
            res.extend(field.get_names())
        self._field_names = res
        return res

    @property
    def field_count(self) -> int:
        """Return length of field list."""
        res = 0
        for field in self._fields:
            res += len(field)
        return res

    # def field_name(self, index: int) -> str:
    #     """Return field name at index."""
    #     assert 0 <= index < len(self._fields), 'Field index out of range!'
    #     return self._fields[index]

    def field_pos(self, name: str) -> int:
        """Return position of field."""
        field_names = self.field_names
        assert name in field_names, 'Name "{}" not in fields!'.format(name)
        return field_names.index(name)

    # ---- read data from binary file ----
    @staticmethod
    def get_version(data: bytes = b'') -> int:
        """Get file version."""
        if len(data) < 4:
            return 0
        return struct.unpack('<I', data[0:4])[0]

    def read_dataset(self) -> Optional[TDTADataSet]:
        """Read single dataset from binary data defined by fields."""
        if self._dta.bytes_to_read < self._dataset_length:
            return None

        ds: TDTADataSet = array('d')
        for f in self._fields:
            ds.extend(f.read(self._dta))
        return ds

    # ---- calculate fields ----
    @staticmethod
    def calc_linear_data(value: float, m: float, n: float, precision: int = 10) -> float:
        """Calculate from slope and offset."""
        res: float = value * m + n
        return round(res * precision) / precision

    @classmethod
    def calc_flow_rate_grundfoss_vfs5_5_100(cls, ai1: float) -> float:
        """Calculate flow from analog input signals for Grundfoss VFS 5-100."""
        # Werte fuer Durchflussmesser Grundfoss VFS 5-100
        # der Sensor hat "nur" 0.5l/min Aufloesung
        # alle Werte unter 0,5V sind als Durchfluss=0.0l/min zu werten
        flow: float = 0.0
        if 0.5 <= ai1 <= 5.0:
            flow = cls.calc_linear_data(1, ai1 * 95.0 / 3.0, -65.0 / 6.0, 2)
        return flow

    # def to_csv(self, file: IO) -> None:
    #     """Create CSV representation."""
    #     writer = csv.writer(file)
    #     writer.writerow(self._fields)
    #     writer.writerows(self._data)

    @staticmethod
    def calc_qth(flow: float, sp_heat: float) -> float:
        """Calculate thermal power from flow and spread in head circulation."""
        # Qth = Durchfluss->at(l/min) * Spreizung->at(K) / 60 * c->at(kJ/kg) * Dichte->at(kg/l)
        #   c(Wasser) = 4.18kJ/kg bei 30 Grad C
        #   Dichte = 1.0044^-1 kg/l bei 30 Grad C
        qth: float = flow * sp_heat / 60 * 4.18 * 0.9956
        return round(qth * 100) / 100

    # ---- dataset access (iterator) ----
    def __len__(self) -> int:
        """Return number of datasets."""
        return len(self._data)

    def __getitem__(self, idx: int) -> TDTADataSet:
        """Return dataset at position idx."""
        return self._data[idx]

    def __iter__(self):
        """Return iterator for datasets."""
        self._iter_pos = 0
        return self

    def __next__(self) -> TDTADataSet:
        if self._iter_pos >= len(self._data):
            raise StopIteration
        res = self._data[self._iter_pos]
        self._iter_pos += 1
        return res

    # ---- merge data ----
    def __iadd__(self, other):
        """Add datasets of other to current instance."""
        # is other empty?
        if len(other) == 0:
            return self

        # setup self if not yet done
        if self.version == 0:
            self._version = other.version
            self._fields.extend(other.fields)

        # check compatibility
        if self.version != other.version:
            raise DtaViolation(f'Error: self (0x{self.version:04x}) and other (0x{other.version:04x}) '
                               f'have different versions!')
        if self.field_names != other.field_names:
            raise DtaViolation('Error: self and other have different fields!')

        if len(self._data) == 0:
            self._data.extend(other)
        else:
            start: int = self._data[0][0]
            end: int = self._data[-1][0]

            if other[-1][0] < start:
                self._data = list(other) + self._data
            elif other[0][0] < start:
                other_before: List[TDTADataSet] = [ds for ds in other if ds[0] < start]
                self._data = other_before + self._data

            if other[0][0] > end:
                self._data.extend(other)
            elif other[-1][0] > end:
                other_after: List[TDTADataSet] = [ds for ds in other if ds[0] > end]
                self._data.extend(other_after)

        return self

    def compatible(self, other) -> bool:
        """Check whether other can be merged with self."""
        if self.version == 0:
            return True
        if other.version == 0:
            return True
        if self.version != other.version:
            return False
        if self.field_names != other.field_names:
            return False
        return True

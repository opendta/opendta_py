# ----------------------------------------------------------------------
# Copyright (C) 2018  opendta@gmx.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

"""Process DTA file version 9003."""
from typing import Optional

from .dtafile import DtaFile, TDTADataSet, DtaViolation
from .dtafield import DtaFieldTimestamp, DtaFieldAnalog, DtaFieldEnum, DtaFieldDigitalBit, DtaFieldDigital, DtaRGB


class DtaFile9003(DtaFile):
    """Process DTA file version 9003."""

    # debug file processing
    # DUMP_HEADER: bool = True
    DUMP_HEADER: bool = False

    def __init__(self, data: bytes = b''):
        super().__init__(data)

        datasets_to_read: int = 0
        fielddef_size: int = 0

        # read header
        if self._dta.bytes_to_read >= 6:
            fielddef_size = self._dta.get_uint32()
            datasets_to_read = self._dta.get_uint16()
        self._log('')
        self._log('size of field def: 0x{:04X}'.format(fielddef_size))
        self._log('datasets to read: {}'.format(datasets_to_read))

        # read field definitions
        if fielddef_size > 0:
            self.read_fields_def(fielddef_size)

        if len(self._fields) > 0:
            # reduce dataset count during debug
            if self.DUMP_HEADER:  # pragma: no cover
                datasets_to_read = 10

            # read datasets
            for i in range(datasets_to_read):
                ds: Optional[TDTADataSet] = self.read_dataset()
                if ds is None:
                    break
                self._data.append(ds)

        # clean up
        del self._dta

    def _log(self, s: str) -> None:
        """Print debug message."""
        if self.DUMP_HEADER:  # pragma: no cover
            print(s)

    def _get_string(self) -> str:
        """Read string from binary data and remove 'Text_'."""
        res = self._dta.get_string()
        if res.startswith('Text_'):
            res = res[5:]
        return res

    def read_fields_def(self, fielddef_size: int) -> None:
        """Read field definitions."""
        if self._dta.bytes_to_read < fielddef_size - 8:
            return None

        # some vars
        ds_len: int = 0
        end_of_header: int = self._dta.pos + fielddef_size - 2
        category: str = ''
        name: str = ''
        count: int = 0
        rgb: DtaRGB = DtaRGB()
        visible: bool = True
        factory_only: bool = False

        self._fields.append(DtaFieldTimestamp(name='Zeitstempel'))
        ds_len += 4

        # dataset length
        self._dataset_length = self._dta.get_uint16()
        self._log('dataset length: {}'.format(self._dataset_length))

        self._log('field type, field name, field data')
        while self._dta.pos < end_of_header and ds_len < self._dataset_length:
            field_id: int = self._dta.get_byte()
            field_type: int = field_id & 0x0F

            visible = False if field_id & 0x40 else True
            factory_only = True if field_id & 0x20 else False

            if field_type == 0:
                # group
                category = self._get_string()
                self._log('0x{:02X}, {}'.format(field_id, category))

            elif field_type == 1:
                # analog fields
                name = self._get_string()
                rgb = DtaRGB(*self._dta.get_rgb())
                factor: int = 10
                if field_id & 0x80:
                    factor = self._dta.get_uint16()
                field = DtaFieldAnalog(category=category, name=name, factor=float(factor), color=rgb,
                                       visible=visible, factory_only=factory_only)
                self._fields.append(field)
                self._log('0x{:02X}, {}'.format(field_id, repr(field)))
                ds_len += 2

            elif field_type == 2 or field_type == 4:
                # digital fields
                dig: DtaFieldDigital = DtaFieldDigital(category=category)
                count = self._dta.get_byte()

                # visibility for each entry
                visibility: int = 0xFFFF
                if field_id & 0x40:
                    visibility = self._dta.get_uint16()

                # factory_only for each entry
                factory_only_all: int = 0
                if field_id & 0x20:
                    factory_only_all = self._dta.get_uint16()

                # in/out flag for each entry
                ios: int = 0
                if field_id & 0x04:
                    ios = self._dta.get_uint16()
                elif field_id & 0x80:
                    ios = 0xFFFF

                self._log('0x{:02X}, IO with {} items, visibility=0x{:04X}, factory_only=0x{:04X}, ios=0x{:04X}'.format(
                    field_id, count, visibility, factory_only, ios))

                for i in range(count):
                    name = self._get_string()
                    rgb = DtaRGB(*self._dta.get_rgb())
                    bit: DtaFieldDigitalBit = DtaFieldDigitalBit(pos=i, category=category, name=name, color=rgb)
                    bit.set_input(ios)
                    bit.set_visible(visibility)
                    bit.set_factory_only(factory_only_all)
                    dig.bits.append(bit)
                    self._log('  {}, RGB={}'.format(name, rgb))
                self._log('IO end')
                self._fields.append(dig)
                ds_len += 2

            elif field_type == 3:
                # enum field
                name = self._dta.get_string()
                count = self._dta.get_byte()
                ds_len += 2

                enum: DtaFieldEnum = DtaFieldEnum(category=category, name=name, visible=visible,
                                                  factory_only=factory_only)
                self._fields.append(enum)
                self._log('0x{:02X}, ENUM "{}" with {} items'.format(field_id, name, count))
                for i in range(count):
                    item_text: str = self._get_string()
                    enum.value_strings.append(item_text)
                    self._log('   {} = {}'.format(i, item_text))

            else:
                # unknown field
                raise DtaViolation('DTA v9003 - unknown field type {:02X}!'.format(field_type))  # pragma: no cover

        # verify dataset length
        assert ds_len == self._dataset_length, \
            'Calculated dataset length ({}) is not the same as stated by file ({})!'.format(ds_len, self._dataset_length)
        # verify file position
        assert self._dta.pos == end_of_header, \
            'File position (0x{:04X}) is not at end of header (0x{:04X})!'.format(self._dta.pos, end_of_header)

        self._log('')

    def read_dataset(self) -> Optional[TDTADataSet]:
        """Read single dataset."""
        ds = super().read_dataset()
        self._log(str(ds))
        return ds
